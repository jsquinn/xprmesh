XprMesh Copyright 2024 Jonathon Quinn

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

XprMesh is intended to be a test concept of multiple 'nodes' (processes)
communicating and calculating simple arithmetic expressions which
can depend on the result of expressions calculated by another node.

A central server 'a' node tracks all expressions and writes configuration
to storage. Nodes and expressions can be added via the server console
and expressions are assigned a node to be calculated on.

This project was an informal trial of rust in a reasonable project,
including non-trival data structures and ownership, async/tokio socket IO,
writing to files, serde_json serialisation and language independent
problems such as tracking distributed dependencies (not done well yet),
IO buffering etc.

Next ideas:
 cleanly delete clients in server process
 Track Client dependencies for incremental updates
 Clients prod dependents for values
 Client persistence of expr/ports
 u64 Node ids and ubiquitous relation persistence
 TLS authentication

