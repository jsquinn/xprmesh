pub type Xid = u64;
pub type Xvalue = i64;
pub type XidSet = std::vec::Vec<Xid>;
pub type XvalueMap = std::collections::HashMap<Xid, Xvalue>;

pub fn read_number(expr: &str, end: &mut usize) -> Option<u64> {
    let mut index = expr.char_indices();
    let mut ci = index.next();
    *end = 0;
    let mut value: u64 = 0;
    let mut found = false;
    loop {
        if let Some((i, c)) = ci {
            if let Some(d) = c.to_digit(10) {
                found = true;
                *end = i+1;
                value = value * 10 + (d as u64);
                ci = index.next();
            } else if found {
                return Some(value)
            } else {
                return None
            }
        } else if found {
            return Some(value)
        } else {
            return None
        }
    }
}

fn evaluate_sub_expression(expr: &str, end: &mut usize, expr_values: &XvalueMap) ->std::result::Result<Option<Xvalue>, &'static str> {
    let mut index = expr.char_indices();
    let mut ci = index.next();

    loop {
        if let Some((_, c)) = ci {
            if c.is_whitespace() || c == ',' {
                ci = index.next()
            } else {
                break;
            }
        } else {
            return Err("Expression ended incomplete");
        }
    }

    match ci {
        None => Err("Expression ended incomplete"),
        Some((i, c)) => {
            match c {
                '+'|'*'|'-' => {
                    let mut sub_start = i+1;
                    let mut sub_end = i+1;
                    let vo1 = evaluate_sub_expression(&expr[sub_start..], &mut sub_end, expr_values)?;
                    if let Some(v1) = vo1 {
                        sub_start += sub_end;
                        let vo2 = evaluate_sub_expression(&expr[sub_start..], &mut sub_end, expr_values)?;
                        if let Some(v2) = vo2 {
                            *end = sub_start + sub_end;
                            match c {
                                '+' => Ok(Some(v1 + v2)),
                                '*' => Ok(Some(v1 * v2)),
                                '-' => Ok(Some(v1 - v2)),
                                _ => Err("Internal operator error"),
                            }
                        } else {
                            Ok(None)
                        }
                    } else {
                       Ok(None)
                    }
                },
                '0'..='9' => {
                    let mut sub_end = 0;
                    let opt_value = read_number(&expr[i..], &mut sub_end);
                    *end = i+sub_end;
                    if let Some(v) = opt_value {
                        Ok(Some(v as Xvalue))
                    } else {
                        Err("Internal number error")
                    }
                },
                '#' => {
                    let mut sub_end = 0;
                    let opt_value = read_number(&expr[i+1..], &mut sub_end);
                    *end = i+1+sub_end;
                    if let Some(v) = opt_value {
                        if let Some(x) = expr_values.get(&v) {
                            Ok(Some(*x))
                        } else {
                            // expr value isn't defined yet
                            Ok(None)
                        }
                    } else {
                        Err("Invalidate ref number")
                    }
                },
                _ => Err("Unexpected Character"),
            }
        },
    }
}

pub fn evaluate_expression(expr: &str, expr_values: &XvalueMap) ->std::result::Result<Option<Xvalue>, &'static str> {
    let mut end : usize = 0;
    let result = evaluate_sub_expression(expr, &mut end, expr_values)?;
    match result {
        Some(_) => {
            if end != expr.len() {
                return Err("Expression completed early");
            }
            return Ok(result);
        },
        None => return Ok(result),
    }
}

fn extract_ref_sub_expression(expr: &str, end: &mut usize, deps_found: &mut XidSet) ->std::result::Result<(), &'static str> {
    let mut index = expr.char_indices();
    let mut ci = index.next();

    loop {
        if let Some((_, c)) = ci {
            if c.is_whitespace() || c == ',' {
                ci = index.next()
            } else {
                break;
            }
        } else {
            return Err("Expression ended incomplete");
        }
    }

    match ci {
        None => Err("Expression ended incomplete"),
        Some((i, c)) => {
            match c {
                '+'|'*'|'-' => {
                    let mut sub_start = i+1;
                    let mut sub_end = i+1;
                    extract_ref_sub_expression(&expr[sub_start..], &mut sub_end, deps_found)?;
                    sub_start += sub_end;
                    extract_ref_sub_expression(&expr[sub_start..], &mut sub_end, deps_found)?;
                    *end = sub_start + sub_end;
                    Ok(())
                },
                '0'..='9' => {
                    let mut sub_end = 0;
                    let opt_value = read_number(&expr[i..], &mut sub_end);
                    *end = i+sub_end;
                    if let Some(_v) = opt_value {
                        Ok(())
                    } else {
                        Err("Internal number error")
                    }
                },
                '#' => {
                    let mut sub_end = 0;
                    let opt_value = read_number(&expr[i+1..], &mut sub_end);
                    *end = i+1+sub_end;
                    if let Some(v) = opt_value {
                        if !deps_found.contains(&v) {
                            deps_found.push(v);
                            deps_found.sort();
                        }
                        Ok(())
                    } else {
                        Err("Invalidate ref number")
                    }
                },
                _ => Err("Unexpected Character"),
            }
        },
    }
}

pub fn extract_ref_expression(expr: &str) ->std::result::Result<XidSet, &'static str> {
    let mut refs = XidSet::new();
    let mut end : usize = 0;
    extract_ref_sub_expression(expr, &mut end, &mut refs)?;
    if end != expr.len() {
        return Err("Expression completed early");
    }
    Ok(refs)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_number() {
        let mut end : usize = 9;
        assert_eq!(read_number("", &mut end), None);
        assert_eq!(end, 0);
        end = 9;
        assert_eq!(read_number(" ", &mut end), None);
        assert_eq!(end, 0);
        end = 9;
        assert_eq!(read_number("5", &mut end), Some(5));
        assert_eq!(end, 1);
        end = 9;
        assert_eq!(read_number("5 ", &mut end), Some(5));
        assert_eq!(end, 1);
        end = 9;
        assert_eq!(read_number("1234567890", &mut end), Some(1234567890));
        assert_eq!(end, 10);
        end = 9;
        assert_eq!(read_number("1234567890+", &mut end), Some(1234567890));
        assert_eq!(end, 10);
    }

    #[test]
    fn test_expression() {
        let empty = XvalueMap::new();
        assert_eq!(evaluate_expression("", &empty), Err("Expression ended incomplete"));
        assert_eq!(evaluate_expression(" ", &empty), Err("Expression ended incomplete"));
        assert_eq!(evaluate_expression("1", &empty), Ok(Some(1)));
        assert_eq!(evaluate_expression("1234567890", &empty), Ok(Some(1234567890)));
        assert_eq!(evaluate_expression("1 ", &empty), Err("Expression completed early"));
        assert_eq!(evaluate_expression("1+1", &empty), Err("Expression completed early"));
        assert_eq!(evaluate_expression("1,2", &empty), Err("Expression completed early"));
        assert_eq!(evaluate_expression("+1,2", &empty), Ok(Some(3)));
        assert_eq!(evaluate_expression("*7,8", &empty), Ok(Some(56)));
        assert_eq!(evaluate_expression("-7,8", &empty), Ok(Some(-1)));
        assert_eq!(evaluate_expression("*-7,8,3", &empty), Ok(Some(-3)));
        assert_eq!(evaluate_expression("* - 6 , 8 , 53", &empty), Ok(Some(-106)));
        assert_eq!(evaluate_expression("* - 6 , 8 , +20 3", &empty), Ok(Some(-46)));
        assert_eq!(evaluate_expression("* - 624 , 813 , +20231 34324", &empty), Ok(Some(-10310895)));

        assert_eq!(evaluate_expression("#1", &empty), Ok(None));
        assert_eq!(evaluate_expression("#156345", &empty), Ok(None));
        assert_eq!(evaluate_expression(",,,,#156345", &empty), Ok(None));
        assert_eq!(evaluate_expression("+1,#1", &empty), Ok(None));
        assert_eq!(evaluate_expression("+#1,1", &empty), Ok(None));
        assert_eq!(evaluate_expression("+#1,#2", &empty), Ok(None));

        let values = XvalueMap::from([(1, 10), (2, 20)]);
        assert_eq!(evaluate_expression("#1", &values), Ok(Some(10)));
        assert_eq!(evaluate_expression("#2", &values), Ok(Some(20)));
        assert_eq!(evaluate_expression("+#1,#2", &values), Ok(Some(30)));
        assert_eq!(evaluate_expression("*#1,#2", &values), Ok(Some(200)));
        assert_eq!(evaluate_expression("*#3,#2", &values), Ok(None));
        assert_eq!(evaluate_expression("*#1,#3", &values), Ok(None));
        assert_eq!(evaluate_expression("+*#1,#3,#2", &values), Ok(None));
        assert_eq!(evaluate_expression("+*#1,#2,#3", &values), Ok(None));
    }

    #[test]
    fn test_extract_refs() {
        assert_eq!(extract_ref_expression(""), Err("Expression ended incomplete"));
        assert_eq!(extract_ref_expression(" "), Err("Expression ended incomplete"));
        assert_eq!(extract_ref_expression("1"), Ok(vec![]));
        assert_eq!(extract_ref_expression("1234567890"), Ok(vec![]));
        assert_eq!(extract_ref_expression("1 "), Err("Expression completed early"));
        assert_eq!(extract_ref_expression("1+1"), Err("Expression completed early"));
        assert_eq!(extract_ref_expression("1,2"), Err("Expression completed early"));
        assert_eq!(extract_ref_expression("+1,2"), Ok(vec![]));
        assert_eq!(extract_ref_expression("*7,8"), Ok(vec![]));
        assert_eq!(extract_ref_expression("-7,8"), Ok(vec![]));
        assert_eq!(extract_ref_expression("*-7,8,3"), Ok(vec![]));
        assert_eq!(extract_ref_expression("* - 6 , 8 , 53"), Ok(vec![]));
        assert_eq!(extract_ref_expression("* - 6 , 8 , +20 3"), Ok(vec![]));
        assert_eq!(extract_ref_expression("* - 624 , 813 , +20231 34324"), Ok(vec![]));

        assert_eq!(extract_ref_expression("#1"), Ok(vec![1]));
        assert_eq!(extract_ref_expression("#156345"), Ok(vec![156345]));
        assert_eq!(extract_ref_expression(",,,,#156345"), Ok(vec![156345]));
        assert_eq!(extract_ref_expression("+12,#1"), Ok(vec![1]));
        assert_eq!(extract_ref_expression("+#1,12"), Ok(vec![1]));

        assert_eq!(extract_ref_expression("#1"), Ok(vec![1]));
        assert_eq!(extract_ref_expression("#2"), Ok(vec![2]));
        assert_eq!(extract_ref_expression("+#1,#2"), Ok(vec![1,2]));
        assert_eq!(extract_ref_expression("*#1,#2"), Ok(vec![1,2]));
        assert_eq!(extract_ref_expression("-#3,#2"), Ok(vec![2,3]));
        assert_eq!(extract_ref_expression("*#1,#3"), Ok(vec![1,3]));
        assert_eq!(extract_ref_expression("+*#1,#3,#2"), Ok(vec![1,2,3]));
        assert_eq!(extract_ref_expression("++#1,#2,#3"), Ok(vec![1,2,3]));
        assert_eq!(extract_ref_expression("*+#300,#299,#301"), Ok(vec![299,300,301]));
    }
}
