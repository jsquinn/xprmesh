use super::xprlog::XprLog;
use super::xlog;

use super::xpression::Xid;
use super::xpression::Xvalue;
use super::xpression::XidSet;

struct XprExpression {
    expression: String,
    dep_ids: XidSet,
    local_value: std::result::Result<Option<Xvalue>, &'static str>,
}

pub struct XprSet {
    xprs: std::collections::HashMap<Xid, XprExpression>,
    values: std::collections::HashMap<Xid, Xvalue>,
}

impl XprSet {
    pub fn new() ->XprSet {
        XprSet{
            xprs: std::collections::HashMap::new(),
            values: std::collections::HashMap::new(),
        }
    }

    pub fn show(self: &Self, log: &XprLog) {
        let mut global_value_str = String::new();
        for (xid, xpr) in self.xprs.iter() {
            match self.values.get(xid) {
                Some(v) => global_value_str = format!(" [{}]", v),
                None => global_value_str.clear(),
            }
            match xpr.local_value {
                Ok(Some(v)) => {
                    xlog!(log, "{} = {} = {}{}", xid, xpr.expression, v, global_value_str);
                },
                Ok(None) => {
                    xlog!(log, "{} = {} = None{}", xid, xpr.expression, global_value_str);
                },
                Err(s) => {
                    xlog!(log, "{} = {} = {}{}", xid, xpr.expression, s, global_value_str);
                },
            }
        }

        for (xid, value) in self.values.iter() {
            if !self.xprs.contains_key(xid) {
                xlog!(log, "?#{} = remote [{}]", xid, value);
            }
        }
    }

    pub fn insert_xpr(self: &mut Self, xid: Xid, expression: String, deps: XidSet, values_changed: &mut XidSet, log: &XprLog) {
        if self.xprs.contains_key(&xid) {
            panic!("insert_xpr xid matches existing xpr!");
        } else {
            self.xprs.insert(xid.clone(), XprExpression{ expression: expression,
                    dep_ids: deps, local_value: Err("Unintialised added") });

            values_changed.push(xid.clone());
            self.recalc_xpr(&xid, values_changed, log);
        }
    }

    pub fn remove_xpr(self: &mut Self, xid: &Xid, values_changed: &mut XidSet, log: &XprLog) {
        if self.xprs.contains_key(xid) {
            self.xprs.remove(xid);
            self.values.remove(xid);
            values_changed.push(xid.clone());
            self.handle_value_changed(xid, values_changed, log);
        } else {
            panic!("remove_xpr xid does not match existing xpr!");
        }
    }

    pub fn is_local_xid(self: &Self, xid: &Xid) -> bool {
        self.xprs.contains_key(xid)
    }

    pub fn get_local_xids(self: &Self) -> XidSet {
        self.xprs.keys().cloned().collect()
    }

    pub fn get_local_value(self: &Self, xid: &Xid) ->Option<Xvalue>{
        if let Some(xpr) = self.xprs.get(xid) {
            if let Ok(opt_value) = xpr.local_value {
                opt_value
            } else {
                None
            }
        } else {
            None
        }
    }

    pub fn insert_peer_value(self: &mut Self, xid: Xid, value: Xvalue, values_changed: &mut XidSet, log: &XprLog) {
        if self.xprs.contains_key(&xid) {
            panic!("insert_peer_value xid matches local xpr!");
        } else {
            self.values.insert(xid, value);
            self.handle_value_changed(&xid, values_changed, log);
        }
    }

    pub fn remove_peer_value(self: &mut Self, xid: &Xid, values_changed: &mut XidSet, log: &XprLog) {
        if self.xprs.contains_key(xid) {
            panic!("remove_peer_value xid matches local xpr!");
        } else {
            self.values.remove(xid);
            self.handle_value_changed(xid, values_changed, log);
        }
    }

    fn calc_expression(xid: &Xid, xpr: &mut XprExpression, values: &mut std::collections::HashMap<Xid, Xvalue>, log: &XprLog) -> bool{
        let old_value = xpr.local_value;
        xpr.local_value = super::xpression::evaluate_expression(&xpr.expression, &values);
        match xpr.local_value {
            Ok(Some(v)) => {
                xlog!(log, "#{} = {}", xid, v);
                values.insert(xid.clone(), v);
            },
            Ok(None) => {
                xlog!(log, "#{} = None", xid);
                values.remove(xid);
            },
            Err(s) => {
                xlog!(log, "#{} = {}", xid, s);
                values.remove(xid);
            },
        }
        old_value != xpr.local_value
    }

    fn recalc_xpr(self: &mut Self, xid: &Xid, values_changed: &mut XidSet, log: &XprLog) {
        xlog!(log, "recalcing {}", xid);
        if let Some(xpr) = self.xprs.get_mut(xid) {
            if Self::calc_expression(xid, xpr, &mut self.values, log) {
                values_changed.push(xid.clone());
                self.handle_value_changed(xid, values_changed, log);
            }
        }
    }

    fn handle_value_changed(self: &mut Self, xid: &Xid, values_changed: &mut XidSet, log: &XprLog) {
        xlog!(log, "{} value changed", xid);
        let mut changed = XidSet::new();
        let mut dependents = std::vec::Vec::new();
        for (dxid, xpr) in self.xprs.iter() {
            if xpr.dep_ids.contains(xid) {
                dependents.push(dxid.clone());
            }
        }

        for dxid in dependents.iter() {
            if Self::calc_expression(dxid, self.xprs.get_mut(dxid).unwrap(), &mut self.values, log) {
                changed.push(dxid.clone())
            }
        }

        values_changed.extend_from_slice(&changed);
        for cxid in changed.iter() {
            self.handle_value_changed(cxid, values_changed, log);
        }
    }

    pub fn web(self: &Self) -> String {
        let mut result = String::new();
        let mut global_value_str = String::new();
        for (xid, xpr) in self.xprs.iter() {
            match self.values.get(xid) {
                Some(v) => global_value_str = format!(" [{}]", v),
                None => global_value_str.clear(),
            }
            match xpr.local_value {
                Ok(Some(v)) => {
                    result += &format!("<li>{} = {} = {}{}</li>", xid, xpr.expression, v, global_value_str);
                },
                Ok(None) => {
                    result += &format!("<li>{} = {} = None{}</li>", xid, xpr.expression, global_value_str);
                },
                Err(s) => {
                    result += &format!("<li>{} = {} = {}{}</li>", xid, xpr.expression, s, global_value_str);
                },
            }
        }

        for (xid, value) in self.values.iter() {
            if !self.xprs.contains_key(xid) {
                result.push_str(&format!("<li>?#{} = remote [{}]</li>", xid, value));
            }
        }

        result
    }
}

