/*
 * keystrdb supports a simple string dictionary stored in file
 * and cached in memory
 *
 * the db file is ascii text new line separated
 * key and values strings are space separated and
 * cannot contain new lines, spaces or control characters themselves.
 *
 * Gaps with only white space can appear in the file and will be
 * replaced with inserted entries. Deleted entries will be replaced with a gap.
 * */
#![allow(dead_code)]

use std::io::Result;
use std::fs::File;
use std::io::Write;
use std::io::BufReader;
use std::io::BufRead;
use std::io::SeekFrom;
use std::io::Seek;
use std::io::Error;
use std::io::ErrorKind;
use std::path::Path;
// tried btree_slab::BTreeMap - seemed ~10% slower to insert
//use btree_slab::BTreeMap;
use std::collections::BTreeMap;
use std::collections::BTreeSet;

struct KeyStrEntry
{
    value: String,
    location: u64,
    len: u64
}

type BTreeRange<'a, K, V> = std::collections::btree_map::Range<'a, K, V>;
//type BTreeRange<'a, K, V> = btree_slab::generic::map::Range<'a, K, V, slab::Slab<btree_slab::generic::node::Node<K, V>>>;

pub struct KeyStrStartIter<'a>
{
    range: BTreeRange<'a, String, KeyStrEntry>,
}

impl <'a> Iterator for KeyStrStartIter<'a> {
    type Item = (&'a String, &'a String);

    fn next(&mut self) -> Option<Self::Item> {
        match self.range.next() {
            Some((k, v)) => Some((k, &v.value)),
            None => None,
        }
    }
}

trait SeekReadWriteSync: std::io::Seek + std::io::Read + std::io::Write+Send {
    fn peek_data(&self) -> String;
    fn sync_data(&self) -> Result<()>;
}

impl SeekReadWriteSync for std::io::Cursor<std::vec::Vec<u8>> {
    fn peek_data(&self) -> String {
        std::str::from_utf8(self.get_ref().as_slice()).unwrap().to_string()
    }

    fn sync_data(&self) -> Result<()> {
        Ok(())
    }
}

impl SeekReadWriteSync for std::fs::File {
    fn peek_data(&self) -> String {
        "None".to_string()
    }

    fn sync_data(&self) -> Result<()> {
        self.sync_data()
    }
}

pub struct KeyStrDB {
    stream: Box<dyn SeekReadWriteSync>,
    stream_end: u64,
    values : BTreeMap<String, KeyStrEntry>,
    gaps: BTreeMap<u64, BTreeSet<u64>>
}

impl KeyStrDB {
    fn eio<T>(msg: &str) -> std::io::Result<T> {
        Err(Error::new(ErrorKind::InvalidInput, msg))
    }

    fn check_key(key: &str) ->Result<()> {
        if key.len() == 0 {
            return Self::eio("Empty key");
        }

        for c in key.chars() {
            match c {
                '\\' => (),
                ' ' => (),
                '\n' => (),
                '\r' => (),
                '\t' => (),
                '!'..='~'=> (),
                _ => return Self::eio("Out of range character"),
            }
        }
        return Ok(());
    }

    fn escape_len(s: &str) -> Result<usize> {
        let mut len : usize = 0;

        for c in s.chars() {
            match c {
                '\\' => len+=2,
                ' ' => len+=2,
                '\n' => len+=2,
                '\r' => len+=2,
                '\t' => len+=2,
                '!'..='~'=> len+=1,
                _ => return Self::eio("Out of range character"),
            }
        }
        return Ok(len);
    }

    fn escape_str(s: &str) -> Result<String> {
        let mut result = String::with_capacity(Self::escape_len(s)?);
        for c in s.chars() {
            match c {
                '\\' => result.push_str("\\\\"),
                ' ' => result.push_str("\\_"),
                '\n' => result.push_str("\\n"),
                '\r' => result.push_str("\\r"),
                '\t' => result.push_str("\\t"),
                '!'..='~'=> result.push(c),
                _ => return Self::eio("Out of range character"),
            }
        }
        Ok(result)
    }

    fn unescape_str(s: &str) -> Result<String> {
        let mut result = String::with_capacity(s.len());
        let mut escaped = false;
        for c in s.chars() {
            if escaped
            {
                escaped = false;
                match c {
                    '\\' => result.push('\\'),
                    '_' => result.push(' '),
                    'n' => result.push('\n'),
                    'r' => result.push('\r'),
                    't' => result.push('\t'),
                    _ => return Self::eio("Unexpected Escape Sequence"),
                }
            }
            else
            {
                match c {
                    '\\' => escaped = true,
                    ' '|'\n'|'\r'|'\t' => return Self::eio("Unexpected Unescaped character"),
                    ' '..='~' => result.push(c),
                    _ => return Self::eio("Out of range character"),
                }
            }
        }
        if escaped {
            return Self::eio("Escape at end of line");
        }
        Ok(result)
    }

    pub fn diag(self: &Self) {
        println!("stream size: {}", self.stream_end);
        println!("values:");
        for (key, value) in self.values.iter() {
            println!("{}->{} @{} *{}", key, value.value, value.location, value.len);
        }

        println!("gaps:");
        for (key, set) in self.gaps.iter() {
            for value in set.iter() {
                println!("@{} *{}", value, key);
            }
        }
    }

    pub fn diag_web(self: &Self) -> String {
        let mut result = format!("stream size: {}", self.stream_end) + "<br>";
        result.push_str(&"values:<br>");
        for (key, value) in self.values.iter() {
            result.push_str(&format!("{}->{} @{} *{}<br>", key, value.value, value.location, value.len));
        }

        result.push_str(&"gaps:<br>");
        for (key, set) in self.gaps.iter() {
            for value in set.iter() {
                result.push_str(&format!("@{} *{}<br>", value, key));
            }
        }
        result
    }

    pub fn sync(self: &Self) -> Result<()> {
        self.stream.sync_data()
    }

    pub fn get_start_with(self: &Self, key_prefix: &str) -> KeyStrStartIter<'_> {
        if key_prefix.len() == 0 {
            return KeyStrStartIter{ range: self.values.range("~".to_string()..) };
        }
        let start = key_prefix.to_string();
        let mut end = key_prefix.to_string();
        let new_char = (end.pop().unwrap() as u8 + 1u8) as char;
        end.push(new_char);
        return KeyStrStartIter{ range: self.values.range((std::ops::Bound::Included(start), std::ops::Bound::Excluded(end))) }
    }

    fn add_gap(gaps: &mut BTreeMap<u64, BTreeSet<u64>>, location: u64, len: u64) {
        if len > 0 {
            if ! gaps.contains_key(&len) {
                gaps.insert(len, BTreeSet::new());
            }
            gaps.get_mut(&len).unwrap().insert(location);
        }
    }

    fn find_gap(gaps: &mut BTreeMap<u64, BTreeSet<u64>>, len: u64) -> Option<KeyStrEntry> {
        let mut best_gap_len = None;
        for (key, set) in gaps.range(len..) {
            if set.len() > 0 {
                best_gap_len = Some(*key);
                break;
            }
        }

        if let Some(gap_len) = best_gap_len {
            // unwrap as there must be a set of size gap_len with at least one entry
            let gap_set = gaps.get_mut(&gap_len).unwrap();
            let loc = *gap_set.iter().next().unwrap();
            gap_set.remove(&loc);
            Some(KeyStrEntry{value:String::new(), location: loc, len: gap_len})
        } else {
            None
        }
    }

    fn read_from_bufstream(buf: &mut dyn BufRead, values: &mut BTreeMap<String, KeyStrEntry>,
                gaps: &mut BTreeMap<u64, BTreeSet<u64>>, stream_end: &mut u64)  -> Result<()> {

        let mut total_len : u64 = 0;
        let mut gap_len : u64 = 0;
        let mut line = String::new();

        // load string values
        loop {
            line.clear();
            let read_len = buf.read_line(&mut line)? as u64;
            if read_len == 0 { break; }
            if line.as_bytes()[(read_len-1) as usize] != b'\n' { break; }

            // println!("Found {} chars", read_len);

            let mut key = String::new();
            let mut value = String:: new();
            let mut key_done = false;

            for c in line.chars() {
                if c <= ' ' {
                    // whitespace
                    if value.len() != 0 {
                        // value set - done
                        break;
                    } else if key.len() != 0 {
                        key_done = true;
                    }
                } else {
                    if key_done {
                        value.push(c);
                    } else {
                        key.push(c);
                    }
                }
            }

            if key.len() != 0 {
                // key and value (possibly empty) found
                // record any gap found prior
                if gap_len > 0 {
                    Self::add_gap(gaps, total_len-gap_len, gap_len);
                    gap_len = 0;
                }
                values.insert(Self::unescape_str(&key)?, KeyStrEntry { value: Self::unescape_str(&value)?, location: total_len, len: read_len});
            } else {
                gap_len += read_len;
            }

            total_len += read_len;
        }

        *stream_end = total_len - gap_len;

        Ok(())
    }

    pub fn open_file(path: &Path) -> Result<KeyStrDB> {
        let file = File::options()
                            .read(true).write(true)
                            .open(path)?;
        let mut values : BTreeMap<String, KeyStrEntry> = BTreeMap::new();
        let mut gaps : BTreeMap<u64, BTreeSet<u64>> = BTreeMap::new();
        let mut stream_end : u64 = 0;

        Self::read_from_bufstream(&mut BufReader::new(&file), &mut values, &mut gaps, &mut stream_end)?;

        Ok(KeyStrDB {
            stream: Box::new(file),
            stream_end: stream_end,
            values: values,
            gaps: gaps
        })
    }

    pub fn open_memory(data: &str) ->Result<KeyStrDB> {
        let mut values : BTreeMap<String, KeyStrEntry> = BTreeMap::new();
        let mut gaps : BTreeMap<u64, BTreeSet<u64>> = BTreeMap::new();
        let mut stream_end : u64 = 0;

        Self::read_from_bufstream(&mut BufReader::new(data.as_bytes()), &mut values, &mut gaps, &mut stream_end)?;

        Ok(KeyStrDB {
            stream: Box::new(std::io::Cursor::new(data.as_bytes().to_vec())),
            stream_end: stream_end,
            values: values,
            gaps: gaps
        })
    }

    fn write_keypair(file: &mut dyn SeekReadWriteSync, loc: u64, len: u64, key_bytes: &[u8], value_bytes: &[u8]) -> Result<u64> {
        assert!(len as usize >= key_bytes.len() + value_bytes.len() + 2);
        file.seek(SeekFrom::Start(loc))?;

        let mut written = 0;
        written += file.write(key_bytes)?;
        written += file.write(b" ")?;
        written += file.write(value_bytes)?;

        // on any spare trailing bytes, replace with \n
        let mut trail = len - written as u64;
        while trail > 0 {
            written += file.write(b"\n")?;
            trail-=1;
        }
        assert!(written == len as usize);

        Ok(written as u64)
    }

    pub fn insert(&mut self, key: String, value: String) -> Result<()> {
        if key.len() == 0 {
            return Self::eio("Empty key");
        }

        let esc_key = Self::escape_str(&key)?;
        let key_bytes = esc_key.as_bytes();
        let esc_value = Self::escape_str(&value)?;
        let value_bytes = esc_value.as_bytes();
        let expected_len = (key_bytes.len() + value_bytes.len() + 2) as u64; // includes space and new line

        if let Some(entry) = self.values.get_mut(&key) {
            self.stream.seek(SeekFrom::Start(entry.location))?;
            if entry.len >= expected_len {
                Self::write_keypair(self.stream.as_mut(), entry.location, entry.len, key_bytes, value_bytes)?;

                // done - update cache
                entry.value = value.to_string();
                return Ok(())
            } else {
                // erase, replace with \n
                let mut trail = entry.len;
                while trail > 0 {
                    self.stream.write(b"\n")?;
                    trail-=1;
                }

                Self::add_gap(&mut self.gaps, entry.location, entry.len);
                self.values.remove(&key);
            }
        }

        // new key value or must find a different gap

        if let Some(gap) = Self::find_gap(&mut self.gaps, expected_len) {
            // gap found
            assert!(gap.len >= expected_len);
            let written = Self::write_keypair(self.stream.as_mut(), gap.location, gap.len, key_bytes, value_bytes)?;
            if written+4 < gap.len {
                self.values.insert(key, KeyStrEntry{value: value.to_string(), location: gap.location, len: written});
                Self::add_gap(&mut self.gaps, gap.location+written, gap.len-written);
            } else {
                self.values.insert(key, KeyStrEntry{value: value.to_string(), location: gap.location, len: gap.len});
            }
        } else {
            // else append to end of file
            let written = Self::write_keypair(self.stream.as_mut(), self.stream_end, expected_len, key_bytes, value_bytes)?;
            self.values.insert(key, KeyStrEntry{value: value.to_string(), location: self.stream_end, len: written});
            self.stream_end += written;
        }

        Ok(())
    }

    pub fn remove(&mut self, key: &str) -> Result<bool> {
        Self::check_key(key)?;

        if let Some(entry) = self.values.get_mut(&key.to_string()) {
            // erase, replace with \n
            let mut trail = entry.len;
            self.stream.seek(SeekFrom::Start(entry.location))?;
            while trail > 0 {
                self.stream.write(b"\n")?;
                trail-=1;
            }

            Self::add_gap(&mut self.gaps, entry.location, entry.len);
            self.values.remove(key);
            Ok(true)
        } else {
            Ok(false)
        }
    }

    pub fn get(&self, key: &str) -> Option<&str> {
        if let Some(entry) = self.values.get(key) {
            Some(&entry.value)
        } else {
            None
        }
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    pub fn test_read() -> Result<()>
    {
        let db = KeyStrDB::open_memory("")?;
        assert_eq!(db.values.len() , 0);
        assert_eq!(db.gaps.len() , 0);
        assert_eq!(db.stream_end , 0);

        let db = KeyStrDB::open_memory("\n")?;
        assert_eq!(db.values.len() , 0);
        assert_eq!(db.gaps.len() , 0);
        assert_eq!(db.stream_end , 0);

        let db = KeyStrDB::open_memory("a")?;
        assert_eq!(db.values.len() , 0);
        assert_eq!(db.gaps.len() , 0);
        assert_eq!(db.stream_end , 0);

        let db = KeyStrDB::open_memory("a\n")?;
        assert_eq!(db.values.len() , 1);
        assert_eq!(db.get("a").unwrap(), "");
        assert_eq!(db.gaps.len() , 0);
        assert_eq!(db.stream_end , 2);

        let db = KeyStrDB::open_memory("a \n")?;
        assert_eq!(db.values.len() , 1);
        assert_eq!(db.get("a").unwrap(), "");
        assert_eq!(db.gaps.len() , 0);
        assert_eq!(db.stream_end , 3);

        let db = KeyStrDB::open_memory(" a \n")?;
        assert_eq!(db.values.len() , 1);
        assert_eq!(db.get("a").unwrap(), "");
        assert_eq!(db.values.get("a").unwrap().location, 0);
        assert_eq!(db.values.get("a").unwrap().len, 4);
        assert_eq!(db.gaps.len() , 0);
        assert_eq!(db.stream_end , 4);

        let db = KeyStrDB::open_memory("a\nb")?;
        assert_eq!(db.values.len() , 1);
        assert_eq!(db.get("a").unwrap(), "");
        assert_eq!(db.gaps.len() , 0);
        assert_eq!(db.stream_end , 2);

        let db = KeyStrDB::open_memory("a b\n")?;
        assert_eq!(db.values.len() , 1);
        assert_eq!(db.get("a").unwrap(), "b");
        assert_eq!(db.gaps.len() , 0);
        assert_eq!(db.stream_end , 4);

        let db = KeyStrDB::open_memory("a  b\n")?;
        assert_eq!(db.values.len() , 1);
        assert_eq!(db.get("a").unwrap(), "b");
        assert_eq!(db.gaps.len() , 0);
        assert_eq!(db.stream_end , 5);

        let db = KeyStrDB::open_memory("a  b \n")?;
        assert_eq!(db.values.len() , 1);
        assert_eq!(db.get("a").unwrap(), "b");
        assert_eq!(db.gaps.len() , 0);
        assert_eq!(db.stream_end , 6);

        let db = KeyStrDB::open_memory("\n \n")?;
        assert_eq!(db.values.len() , 0);
        assert_eq!(db.gaps.len() , 0);
        assert_eq!(db.stream_end , 0);

        let db = KeyStrDB::open_memory("\n \na  b  \n \n")?;
        assert_eq!(db.values.len() , 1);
        assert_eq!(db.get("a").unwrap(), "b");
        assert_eq!(db.values.get("a").unwrap().location, 3);
        assert_eq!(db.values.get("a").unwrap().len, 7);
        assert_eq!(db.gaps.len() , 1);
        assert_eq!(db.stream_end , 3+7);

        let db = KeyStrDB::open_memory("a b\ncd7 ef6\n0 &\n")?;
        assert_eq!(db.values.len() , 3);
        assert_eq!(db.get("a").unwrap(), "b");
        assert_eq!(db.values.get("a").unwrap().location, 0);
        assert_eq!(db.values.get("a").unwrap().len, 4);
        assert_eq!(db.get("cd7").unwrap(), "ef6");
        assert_eq!(db.values.get("cd7").unwrap().location, 4);
        assert_eq!(db.values.get("cd7").unwrap().len, 8);
        assert_eq!(db.get("0").unwrap(), "&");
        assert_eq!(db.values.get("0").unwrap().location, 12);
        assert_eq!(db.values.get("0").unwrap().len, 4);
        assert_eq!(db.gaps.len() , 0);
        assert_eq!(db.stream_end , 16);

        let db = KeyStrDB::open_memory("\n\na b\ncd7 ef6\n  \n0 &\n")?;
        assert_eq!(db.values.len() , 3);
        assert_eq!(db.get("a").unwrap(), "b");
        assert_eq!(db.values.get("a").unwrap().location, 2);
        assert_eq!(db.values.get("a").unwrap().len, 4);
        assert_eq!(db.get("cd7").unwrap(), "ef6");
        assert_eq!(db.values.get("cd7").unwrap().location, 6);
        assert_eq!(db.values.get("cd7").unwrap().len, 8);
        assert_eq!(db.get("0").unwrap(), "&");
        assert_eq!(db.values.get("0").unwrap().location, 17);
        assert_eq!(db.values.get("0").unwrap().len, 4);
        assert_eq!(db.gaps.len() , 2);
        assert_eq!(db.gaps.get(&2).unwrap().len(), 1);
        assert!(db.gaps.get(&2).unwrap().contains(&0));
        assert_eq!(db.gaps.get(&3).unwrap().len(), 1);
        assert!(db.gaps.get(&3).unwrap().contains(&14));
        assert_eq!(db.stream_end , 21);

        let db = KeyStrDB::open_memory("\n\na\n\nb\n\nc\n \nd\n \ne\n\nf\n")?;
        assert_eq!(db.values.len() , 6);
        assert_eq!(db.gaps.len() , 2);
        assert_eq!(db.gaps.get(&1).unwrap().len(), 3);
        assert!(db.gaps.get(&1).unwrap().contains(&4));
        assert!(db.gaps.get(&1).unwrap().contains(&7));
        assert!(db.gaps.get(&1).unwrap().contains(&18));
        assert_eq!(db.gaps.get(&2).unwrap().len(), 3);
        assert!(db.gaps.get(&2).unwrap().contains(&0));
        assert!(db.gaps.get(&2).unwrap().contains(&10));
        assert!(db.gaps.get(&2).unwrap().contains(&14));
        assert_eq!(db.stream_end , 21);

        Ok(())
    }

    fn insert(db: &mut KeyStrDB, key: &str, value: &str) ->Result<()> {
        db.insert(key.to_string(), value.to_string())
    }

    #[test]
    pub fn test_write() -> Result<()> {
        let mut db = KeyStrDB::open_memory("")?;
        insert(&mut db, "a", "b")?;
        assert_eq!(db.stream.peek_data(), "a b\n");
        insert(&mut db, "b", "a")?;
        assert_eq!(db.stream.peek_data(), "a b\nb a\n");
        insert(&mut db, "a", "x")?;
        assert_eq!(db.stream.peek_data(), "a x\nb a\n");
        insert(&mut db, "a", "xy")?;
        assert_eq!(db.stream.peek_data(), "\n\n\n\nb a\na xy\n");
        insert(&mut db, "c", "b")?;
        assert_eq!(db.stream.peek_data(), "c b\nb a\na xy\n");
        insert(&mut db, "d", "bsdfvgwervoerv")?;
        assert_eq!(db.stream.peek_data(), "c b\nb a\na xy\nd bsdfvgwervoerv\n");
        insert(&mut db, "e", "bsdfvgwervoerv")?;
        assert_eq!(db.stream.peek_data(), "c b\nb a\na xy\nd bsdfvgwervoerv\ne bsdfvgwervoerv\n");
        db.remove("d")?;
        assert_eq!(db.stream.peek_data(), "c b\nb a\na xy\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\ne bsdfvgwervoerv\n");
        insert(&mut db, "q", "bsd")?;
        assert_eq!(db.stream.peek_data(), "c b\nb a\na xy\nq bsd\n\n\n\n\n\n\n\n\n\n\n\ne bsdfvgwervoerv\n");


        Ok(())
    }

    #[test]
    pub fn test_escape() -> Result<()> {
        let mut db = KeyStrDB::open_memory("")?;
        insert(&mut db, "\r\t\n \\~", "~\\ \n\t\r")?;
        assert_eq!(db.stream.peek_data(), "\\r\\t\\n\\_\\\\~ ~\\\\\\_\\n\\t\\r\n");

        assert_eq!(insert(&mut db, "", "abc").is_err(), true);
        assert_eq!(db.remove("").is_err(), true);

        Ok(())
    }
}

