#[macro_export]
macro_rules! xlog {
    ($logger:expr, $fmt:expr $(,$args:expr)*) => {{
            $logger.log(&format!($fmt $(, $args)*)); }}
}

struct Ledger {
    log_history: std::vec::Vec<String>,
}

pub struct XprLog {
    ledger: std::sync::Arc<std::sync::Mutex<Ledger>>,
    changed: std::sync::Arc<super::notifyinc::NotifyInc>,
    prefix: String,
}

impl XprLog {
    pub fn new() -> XprLog {
        XprLog{ledger: std::sync::Arc::new(std::sync::Mutex::new(Ledger{log_history: Vec::new()})),
                changed: std::sync::Arc::new(super::notifyinc::NotifyInc::new()),
                prefix: String::new()}
    }

    pub fn clone(self: &Self) -> XprLog {
        XprLog{ledger: self.ledger.clone(), changed: self.changed.clone(), prefix: self.prefix.clone()}
    }

    pub fn set_prefix(self: &mut Self, prefix: String) {
        self.prefix = prefix;
    }

    pub fn log(self: &Self, s: &str) {
        let entry = format!("{}{}", self.prefix, s);
        println!("{}", entry);


        self.ledger.lock().unwrap().log_history.push(entry);
        self.changed.inc();
    }

    pub fn get_changed(self: &Self) -> std::sync::Arc<super::notifyinc::NotifyInc> {
        self.changed.clone()
    }

    pub fn web_table_rows(self: &Self) -> String {
        let mut result = String::new();

        for s in self.ledger.lock().unwrap().log_history.iter() {
            result += "<tr><td>";
            result += s;
            result += "</tr></td>";
        }

        result
    }
}
