use super::xpression::Xid;
use super::xpression::Xvalue;

#[derive(serde::Serialize,serde::Deserialize)]
pub struct Expression {
    pub id: Xid,
    pub expression: String,
    pub dep_ids: std::vec::Vec<Xid>,
    pub interested_clients: std::vec::Vec<String>,
}

#[derive(serde::Serialize,serde::Deserialize)]
#[serde(tag = "type")]
pub enum Message {
    None,
    Hello{ id: String, port:u16 },
    Chat{ text: String },
    InitialClients(std::collections::HashMap<String, u16>),
    Client{id: String, port: Option<u16>},
    InitialExpressions{ expressions: std::vec::Vec<Expression> },
    Expression{xid: Xid, expression: Option<Expression> },
    InitialValues(std::collections::HashMap<String, Xvalue> ),
    Value{xid: Xid, value: Option<Xvalue> },
}

