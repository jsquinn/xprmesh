pub struct NotifyInc {
    notify: std::sync::Arc<tokio::sync::Notify>,
    set: std::sync::Arc<std::sync::atomic::AtomicU64>
}

impl NotifyInc {
    pub fn new() -> NotifyInc {
        NotifyInc{notify: std::sync::Arc::new(tokio::sync::Notify::new()),
            set: std::sync::Arc::new(std::sync::atomic::AtomicU64::new(0))}
    }

    pub fn clone(self: &Self) -> NotifyInc {
        NotifyInc{ notify: self.notify.clone(), set: self.set.clone() }
    }

    pub fn get_inc(self: &Self) -> u64 {
        self.set.load(std::sync::atomic::Ordering::Relaxed)
    }

    pub fn inc(self: &Self) {
        self.set.fetch_add(1, std::sync::atomic::Ordering::Relaxed);
        self.notify.notify_waiters();
    }

    pub async fn wait(self: &Self) {
        self.notify.notified().await
    }

    pub async fn wait_from(self: &Self, from: u64) {
        if self.set.load(std::sync::atomic::Ordering::Relaxed) <= from {
            self.notify.notified().await
        }
    }
}
