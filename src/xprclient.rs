use super::xpression::Xid;
use super::xpression::XidSet;
use super::xprset::XprSet;
use super::xprconnection::Connected;
use super::xprconnection::Connection;
use super::xprmessage;
use std::str::FromStr;
use super::xprlog::XprLog;
use super::xlog;

pub struct XprPeer {
    port: u16,
    conn_out: Connection,
}

#[allow(dead_code)]
pub struct XprClient {
    id: String,
    port: u16,
    xpr_set: XprSet,
    connection: Connection,
    peers: std::collections::HashMap<String, XprPeer>,
    peers_in: std::collections::HashMap<String, bool>,
    peers_out_recv: Option<tokio::sync::watch::Receiver<Vec<String>>>,
    peers_out_send: tokio::sync::watch::Sender<Vec<String>> ,
    xpr_peer_interest: std::collections::HashMap<Xid, std::vec::Vec<String>>,
}

impl XprClient {
    pub fn init(id: &str) ->XprClient {
        let (tx, rx) = tokio::sync::watch::channel(std::vec::Vec::new());
        XprClient{id: id.to_string(),
            port: 0,
            xpr_set: XprSet::new(),
            connection: Connection::init(id),
            peers: std::collections::HashMap::new(),
            peers_in: std::collections::HashMap::new(),
            peers_out_recv: Some(rx),
            peers_out_send: tx,
            xpr_peer_interest: std::collections::HashMap::new(),
        }
    }

    pub fn get_node(self: &Self) -> String {
        self.id.clone()
    }

    pub fn get_outgoing_peer_list(self: &mut Self) -> Option<tokio::sync::watch::Receiver<Vec<String>>> {
        let mut rx = None;
        std::mem::swap(&mut rx, &mut self.peers_out_recv);
        rx
    }

    pub fn get_outgoing_peer_port(self: &mut Self, id: &str) -> u16 {
        if let Some(peer) = self.peers.get(id) {
            peer.port
        } else {
            0
        }
    }

    pub fn open_outgoing_peer(self: &mut Self, id: &str, log: &XprLog) ->Option<Connected> {
        match self.peers.get_mut(id) {
        Some(peer) => {
            let opt_connected = peer.conn_out.open(log);
            if let Some(_) = opt_connected {
                peer.conn_out.send(&xprmessage::Message::Hello{ id: self.id.clone(), port: self.port }, log);
                self.send_all_values_to(id, log);
            }
            opt_connected
        },
        None => None
        }
    }

    pub fn close_outgoing_peer(self: &mut Self, id: &str, log: &XprLog) {
        match self.peers.get_mut(id) {
        Some(peer) => {
            peer.conn_out.close(log);
        },
        None => (),
        }
    }

    pub fn open(self: &mut Self, port: u16, log: &XprLog) ->Option<Connected> {
        let opt_connected = self.connection.open(log);
        if let Some(_) = opt_connected {
            self.port = port;
            self.connection.send(&xprmessage::Message::Hello{ id: self.id.clone(), port: port }, log);
            self.send_value_changes(&self.xpr_set.get_local_xids(), log);
        }
        opt_connected
    }

    pub fn close(self: &mut Self, log: &XprLog) {
        self.connection.close(log)
    }

    fn update_outgoing_peers(self: &mut Self) {
        let mut peers = std::vec::Vec::new();
        for id in self.peers.keys() {
            peers.push(id.clone());
        }
        peers.sort();
        let _ = self.peers_out_send.send(peers);
    }

    pub fn handle_server_msg(self: &mut Self, msg: &xprmessage::Message, log: &XprLog) {
        match msg {
            xprmessage::Message::InitialClients(clients) => {
                let mut lost_peers = std::vec::Vec::new();
                for (id, _) in self.peers.iter() {
                    if !clients.contains_key(id) {
                        lost_peers.push(id.clone());
                    }
                }
                for id in lost_peers.iter() {
                    self.peers.remove(id);
                }
                for (id, port) in clients.iter() {
                    xlog!(log, "Client: {} port {}", id, port);
                    if let Some(peer) = self.peers.get_mut(id) {
                        peer.port = *port;
                    } else {
                        self.peers.insert(id.to_string(), XprPeer{ port: *port, conn_out: Connection::init(id) });
                    }
                }
                self.update_outgoing_peers();
            },
            xprmessage::Message::Client{id, port: opt_port} => {
                match opt_port {
                Some(port) => {
                    if let Some(peer) = self.peers.get_mut(id) {
                        peer.port = *port;
                    } else {
                        self.peers.insert(id.to_string(), XprPeer{ port: *port, conn_out: Connection::init(id) });
                    }
                    xlog!(log, "Client: {} port {}", id, port);
                    self.update_outgoing_peers();
                },
                None => {
                    self.peers.remove(id);
                    xlog!(log, "Client: {} left", id);
                    self.update_outgoing_peers();
                },
                }
            },
            xprmessage::Message::InitialExpressions{expressions} => {
                let removed = self.xpr_set.get_local_xids();
                let mut values_changed = XidSet::new();
                for xid in removed.iter() {
                    self.xpr_set.remove_xpr(xid, &mut values_changed, log);
                }
                self.xpr_peer_interest.clear();
                for e in expressions {
                    self.xpr_set.insert_xpr(e.id, e.expression.clone(), e.dep_ids.clone(), &mut values_changed, log);
                    self.xpr_peer_interest.insert(e.id, e.interested_clients.clone());
                }

                self.send_value_changes(&values_changed, log);
            },
            xprmessage::Message::Expression{xid, expression: opt_expression} => {
                let mut values_changed = XidSet::new();
                if let Some(e) = opt_expression {
                    if self.xpr_set.is_local_xid(xid) {
                        self.xpr_set.remove_xpr(xid, &mut values_changed, log);
                        self.xpr_peer_interest.remove(xid);
                    }
                    self.xpr_set.insert_xpr(xid.clone(), e.expression.clone(), e.dep_ids.clone(), &mut values_changed, log);
                    self.xpr_peer_interest.insert(e.id, e.interested_clients.clone());
                } else {
                    self.xpr_set.remove_xpr(xid, &mut values_changed, log);
                    self.xpr_peer_interest.remove(xid);
                }
                self.send_value_changes(&values_changed, log);
            },
            xprmessage::Message::InitialValues(values) => {
                let mut values_changed = XidSet::new();
                for (xid_str, v) in values.iter() {
                    if let Ok(xid) = Xid::from_str(&xid_str) {
                        if self.xpr_set.is_local_xid(&xid) {
                            xlog!(log, "Received my xpr {} value", xid);
                        } else {
                            self.xpr_set.insert_peer_value(xid, v.clone(), &mut values_changed, log);
                        }
                    } else {
                        xlog!(log, "Could not parse InitialValues xid");
                    }
                }
                self.send_value_changes(&values_changed, log);
            },
            xprmessage::Message::Value{xid, value: opt_value} => {
                if self.xpr_set.is_local_xid(xid) {
                    xlog!(log, "Received my xpr {} value", xid);
                } else {
                    let mut values_changed = XidSet::new();
                    if let Some(value) = opt_value {
                        self.xpr_set.insert_peer_value(xid.clone(), value.clone(), &mut values_changed, log);
                    } else {
                        self.xpr_set.remove_peer_value(xid, &mut values_changed, log);
                    }
                    self.send_value_changes(&values_changed, log);
                }
            },
            _ => { xlog!(log, "Unexpected message"); },
        }
    }

    pub fn handle_peer_in_msg(self: &mut Self, id: &str, msg: &xprmessage::Message, log: &XprLog) {
        match msg {
            xprmessage::Message::InitialValues(values) => {
                let mut values_changed = XidSet::new();
                for (xid_str, v) in values.iter() {
                    if let Ok(xid) = Xid::from_str(&xid_str) {
                        if self.xpr_set.is_local_xid(&xid) {
                            xlog!(log, "Received my xpr {} value", xid);
                        } else {
                            self.xpr_set.insert_peer_value(xid, v.clone(), &mut values_changed, log);
                            xlog!(log, "Received xpr {} value {} from {}", xid, v, id);
                        }
                    } else {
                        xlog!(log, "Could not parse InitialValues xid");
                    }
                }
                self.send_value_changes(&values_changed, log);
            },
            xprmessage::Message::Value{xid, value: opt_value} => {
                if self.xpr_set.is_local_xid(xid) {
                    xlog!(log, "Received my xpr {} value", xid);
                } else {
                    let mut values_changed = XidSet::new();
                    if let Some(value) = opt_value {
                        self.xpr_set.insert_peer_value(xid.clone(), value.clone(), &mut values_changed, log);
                        xlog!(log, "Received xpr {} value {} from {}", xid, value, id);
                    } else {
                        self.xpr_set.remove_peer_value(xid, &mut values_changed, log);
                        xlog!(log, "Removed xpr {} value from {}", xid, id);
                    }
                    self.send_value_changes(&values_changed, log);
                }
            },
            _ => { xlog!(log, "Unexpected message"); },
        }
    }

    pub fn handle_peer_in_open(self: &mut Self, msg: &xprmessage::Message, log: &XprLog) -> Option<String> {
        if let xprmessage::Message::Hello{id, port: _} = msg {
            if *id == self.id {
                xlog!(log, "Peer cannot immitate client");
                None
            } else if let Some(connected) = self.peers_in.get_mut(id) {
                if *connected {
                    None
                } else {
                    // peer is allowed to connect
                    xlog!(log, "Peer {} handle_peer_open successful", id);
                    *connected = true;
                    Some(id.clone())
                }
            } else {
                self.peers_in.insert(id.clone(), true);
                Some(id.clone())
            }
        } else {
            None
        }
    }

    pub fn handle_peer_in_closed(self: &mut Self, id: &str, log: &XprLog) {
        if let Some(connected) = self.peers_in.get_mut(id) {
            xlog!(log, "Peer {} handle_peer_closed", id);
            *connected = false
        }
    }

    fn send_all_values_to(self: &mut Self, id: &str, log: &XprLog) {
        let str_id = id.to_string();
        if let Some(peer) = self.peers.get_mut(id) {
            for xid in self.xpr_set.get_local_xids() {
                if let Some(value) = self.xpr_set.get_local_value(&xid) {
                    if let Some(interest) = self.xpr_peer_interest.get(&xid) {
                        if interest.contains(&str_id) {
                            let value_msg = xprmessage::Message::Value{xid: xid.clone(), value: Some(value)};
                            peer.conn_out.send(&value_msg, log);
                        }
                    }
                }
            }
        }
    }

    fn send_value_changes(self: &mut Self, changes: &XidSet, log: &XprLog) {
        for xid in changes.iter() {
            let value = self.xpr_set.get_local_value(xid);
            let value = xprmessage::Message::Value{xid: xid.clone(), value: value};
            self.connection.send(&value, log);
            if let Some(peer_ids) = self.xpr_peer_interest.get(xid) {
                xlog!(log, "Sending {} to {:?}", xid, peer_ids);

                for peer_id in peer_ids.iter() {
                    if let Some(peer) = self.peers.get_mut(peer_id) {
                        peer.conn_out.send(&value, log);
                    }
                }
            }
        }
        self.xpr_set.show(log);
    }

    pub fn web(self: &Self) -> String {
        let mut result = format!("<p>client {}</p>", self.connection.status());

        for (id, peer) in self.peers.iter() {
            result += &format!("<li>{} port: {} out: {}</li>", id, peer.port, peer.conn_out.status());
        }
        for (id, connected) in self.peers_in.iter() {
            result += &format!("<li>{} in: {}</li>", id, *connected);
        }
        result += &self.xpr_set.web();
        result
    }
}
