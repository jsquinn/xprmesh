use super::xpression::Xid;
use super::xpression::XidSet;
use super::xprset::XprSet;
use super::xprdb;
use super::xprmessage;
use super::xprconnection;
use super::xprlog::XprLog;
use super::xlog;
use super::notifyinc::NotifyInc;

use std::io::Result;

struct XprConnectedClient {
    port: u16,
    connection: xprconnection::Connection,
}

pub struct XprServer {
    node_id: String,
    db: xprdb::XprDB,
    xpr_set: XprSet,
    clients: std::collections::HashMap<String, XprConnectedClient>,
    client_change: NotifyInc,
    value_change: NotifyInc,
}

impl XprServer {
    pub fn init(node_id: String, log: &XprLog) -> Result<XprServer> {
        const FILENAME :&str = "serverdb.txt";
        xlog!(log, "Loading server db from {}", FILENAME);

        let db = xprdb::XprDB::new(&std::path::Path::new(FILENAME))?;
        let mut xs = XprServer{node_id: node_id,
                db: db,
                xpr_set: XprSet::new(),
                clients: std::collections::HashMap::new(),
                client_change: NotifyInc::new(),
                value_change: NotifyInc:: new() };

        let xids = xs.db.get_node_xids(&xs.node_id);
        let expressions = xs.db.get_expressions(&xids);
        let mut values_changed = XidSet::new();

        for xid in xids.iter() {
            match expressions.get(xid) {
                Some(expression) => {
                    xs.xpr_set.insert_xpr(xid.clone(), expression.to_string(), xs.db.get_dependencies(xid), &mut values_changed, log);
                },
                None => {
                    xlog!(log, "Xid {} expression not found", xid);
                },
            }
        }

        xs.broadcast_value_changes(&values_changed, log);

        return Ok(xs)
    }

    /* Operations */

    pub fn get_node(self: &Self) -> String {
        self.node_id.clone()
    }

    pub fn get_clients(self: &Self) -> Vec<String> {
        self.clients.keys().cloned().collect()
    }

    pub fn get_client_port(self: &Self, node: &str) -> Option<u16> {
        self.clients.get(node).map(|client| client.port)
    }

    pub fn add_xpr(self: &mut Self, xid: Xid, expression: &str, nid: &str, log: &XprLog) -> std::result::Result<(), &'static str> {
        if self.db.get_expression(&xid).is_some() {
            return Err("Xpression ID already used");
        }

        let nodeid = if !nid.is_empty() {
                let mut end : usize = 0;
                if let Some(oknid) = Self::read_nodeid(&nid, &mut end) {
                    if end == oknid.len() {
                        oknid
                    } else {
                        return Err("Characters trailing node id");
                    }
                } else {
                    return Err("Bad node id");
                }
            } else {
                String::from(self.node_id.clone())
            };

        let deps = super::xpression::extract_ref_expression(expression)?;

        let mut xidstack = XidSet::new();
        if !self.db.check_not_circular(&xid, &deps, &mut xidstack) {
            return Err("Circular expression denied");
        }
        let changes = self.db.add_xpr(xid, expression.to_string(), nodeid.clone(), deps.clone())?;
        self.process_expression_changes(changes, log);

        Ok(())
    }

    pub fn rm_xpr(self: &mut Self, xid: &Xid, log: &XprLog) -> std::result::Result<(), &'static str> {
        if self.db.get_expression(xid).is_some() {
            let changes = self.db.rm_xpr(xid)?;
            self.process_expression_changes(changes, log);
            Ok(())
        } else {
           Err("Xpression ID not found")
        }
    }

    pub fn ch_xpr(self: &mut Self, xid: &Xid, expression: &str, log: &XprLog) -> std::result::Result<(), &'static str> {
        if self.db.get_expression(xid).is_some() {
            let deps = super::xpression::extract_ref_expression(expression)?;
            let mut xidstack = XidSet::new();
            if !self.db.check_not_circular(xid, &deps, &mut xidstack) {
                return Err("Circular expression denied");
            }

            let changes = self.db.ch_xpr(xid, expression.to_string(), deps)?;
            self.process_expression_changes(changes, log);
            Ok(())
        } else {
           Err("Xpression ID not found")
        }
    }

    /* Client handling */

    fn get_connection(self: &mut Self, id: &str, port: u16, log: &XprLog) -> Option<xprconnection::Connected> {
        if let Some(client) = self.clients.get_mut(id) {
            if client.connection.online() {
                None
            } else {
                client.port = port;
                client.connection.open(log)
            }
        } else {
            let mut client = XprConnectedClient{ port: port, connection: xprconnection::Connection::init(id) };
            let connected = client.connection.open(log);
            self.clients.insert(id.to_string(), client);
            connected
        }
    }

    fn build_opt_expression_message(self: &Self, xid: &Xid) -> Option<xprmessage::Expression> {
        if let Some(expression) = self.db.get_expression(xid) {
            let deps = self.db.get_dependencies(xid);
            let dep_ons = self.db.get_dependent_on(xid);
            let interested = self.db.get_xids_nodeset(&dep_ons);

            Some(xprmessage::Expression{id: xid.clone(), expression: expression,
                    dep_ids: deps, interested_clients: interested})
        } else {
            None
        }
    }

    pub fn handle_client_open(self: &mut Self, msg: &xprmessage::Message, log: &XprLog) -> Option<xprconnection::Connected> {
        let mut new_client = String::new();
        let result = if let xprmessage::Message::Hello{id, port} = msg {
            if *id == self.node_id {
                xlog!(log, "XprClient cannot immitate server");
                None
            } else if let Some(connected) = self.get_connection(id, *port, log) {
                // client is allowed to connect
                new_client = id.to_string();

                let mut expressions = std::vec::Vec::new();
                let mut values = std::collections::HashMap::new();
                let mut fellow_clients = std::collections::HashMap::new();
                let dependent_on_nodes = self.db.get_dependent_on_nodes_for_node(id);
                let dependent_server_xids = self.db.get_local_xids_depended_by_node(&self.node_id, id);

                for depnode in dependent_on_nodes.iter() {
                    xlog!(log, "Client {} dep on {}", id, depnode);
                }

                for (cid, client) in self.clients.iter() {
                    // itself, dependent on clients values and online
                    if cid != id
                            && dependent_on_nodes.contains(cid)
                            && client.connection.online() {
                        fellow_clients.insert(cid.clone(), client.port);
                    }
                }

                for xid in self.db.get_node_xids(id).iter() {
                    if let Some(msg) = self.build_opt_expression_message(xid) {
                        expressions.push(msg);
                    }
                }

                for did in dependent_server_xids.iter() {
                    if let Some(value) = self.xpr_set.get_local_value(did) {
                        values.insert(did.to_string(), value.clone());
                    }
                }

                if let Some(client) = self.clients.get_mut(id) {
                    client.connection.send(&xprmessage::Message::InitialClients(fellow_clients), log);
                    client.connection.send(&xprmessage::Message::InitialExpressions{expressions: expressions}, log);
                    client.connection.send(&xprmessage::Message::InitialValues(values), log);
                }
                Some(connected)
            } else {
                xlog!(log, "XprClient {} already connected", id);
                None
            }
        } else {
            xlog!(log, "XprClient open  unexpected message");
            None
        };

        if !new_client.is_empty() {
            self.broadcast_client_change(&new_client, log);
        }

        result
    }

    pub fn handle_client_msg(self: &mut Self, id: &str, msg: &xprmessage::Message, log: &XprLog) {
        let client = self.clients.get_mut(id).unwrap();
        if client.connection.online() {
            match msg {
            xprmessage::Message::None => {},
            xprmessage::Message::Hello{id, ..}  => { xlog!(log, "XprServer Client {} already hello'd!", id); },
            xprmessage::Message::Chat{text} => {
                xlog!(log, "XprServer Client {} says {}", id, text);
                client.connection.send(&msg, log);
            },
            xprmessage::Message::Value{xid, value: opt_value} => {
                if let Some(node) = self.db.get_xid_node(xid) {
                    if node == id {
                        let mut values_changed = std::vec::Vec::new();
                        if let Some(value) = opt_value {
                            self.xpr_set.insert_peer_value(xid.clone(), value.clone(), &mut values_changed, log);
                        } else {
                            self.xpr_set.remove_peer_value(xid, &mut values_changed, log);
                        }
                        self.broadcast_value_changes(&values_changed, log);
                    } else {
                        xlog!(log, "Received xpr {} value from {} instead of {}", xid, id, node);
                    }
                } else if opt_value.is_none() {
                    // allow receiving cleared value as the node will no longer be recorded
                    let mut values_changed = std::vec::Vec::new();
                    self.xpr_set.remove_peer_value(xid, &mut values_changed, log);
                    self.broadcast_value_changes(&values_changed, log);
                } else {
                    xlog!(log, "Received unexpected xpr {} value from {}", xid, id);
                }
            },
            _ => xlog!(log, "Unexpected message"),
            }
        }
    }

    pub fn handle_client_closed(self: &mut Self, id: &str, log: &XprLog) {
        if id == self.node_id {
            xlog!(log, "XprServer Client cannot imitate server");
        } else if let Some(mut client) = self.clients.remove(id) {
            client.connection.close(log);
            self.broadcast_client_change(id, log);
        }
    }

    /* reactions */

    fn broadcast_value_changes(self: &mut Self, changes: &XidSet, log: &XprLog) {
        let mut sent = XidSet::new();
        for xid in changes.iter() {
            if sent.contains(xid) {
                continue
            }
            sent.push(xid.clone());
            let value = self.xpr_set.get_local_value(xid);

            let peds = self.db.get_dependent_on(xid);
            for ped in peds.iter() {
                if let Some(node) = self.db.get_xid_node(ped) {
                    if let Some(client) = self.clients.get_mut(&node) {
                        client.connection.send(&xprmessage::Message::Value{xid: xid.clone(), value: value}, log);
                    }
                }
            }
        }
        self.value_change.inc();
    }

    fn broadcast_client_change(self: &mut Self, id: &str, log: &XprLog) {
        let port = match self.clients.get(id) {
            Some(client) => if client.connection.online() {
                    Some(client.port)
                } else {
                    None
                },
            _ => None,
        };

        if port.is_some() {
            // only tell clients about connected client updates if they are a dependent
            let dependency_nodes = self.db.get_dependency_nodes_for_node(id);
            for (cid, client) in self.clients.iter_mut() {
                if *cid != id {
                    if dependency_nodes.iter().any(|x| x == cid) {
                        xlog!(log, "Client update {} send to {}", id, cid);
                        client.connection.send(&xprmessage::Message::Client{id: id.to_string(), port: port}, log);
                    }
                }
            }
        } else {
            // tell every one about removed clients...we don't know who previously depended on
            // them
            for (cid, client) in self.clients.iter_mut() {
                if *cid != id {
                    client.connection.send(&xprmessage::Message::Client{id: id.to_string(), port: port}, log);
                }
            }
        }
        self.client_change.inc();
    }

    fn send_clients_changed(self: &mut Self, id: &str, log: &XprLog) {
        if !self.clients.contains_key(id) {
            return
        }

        let mut fellow_clients = std::collections::HashMap::new();
        let dependent_on_nodes = self.db.get_dependent_on_nodes_for_node(id);

        for (cid, client) in self.clients.iter() {
            // itself, dependent on clients values and online
            if cid != id
                    && dependent_on_nodes.contains(cid)
                    && client.connection.online() {
                xlog!(log, "Client set {} send to {}", cid, id);
                fellow_clients.insert(cid.clone(), client.port);
            }
        }

        if let Some(client) = self.clients.get_mut(id) {
            client.connection.send(&xprmessage::Message::InitialClients(fellow_clients), log);
        }
    }

    fn process_expression_changes(self: &mut Self, changes: xprdb::XprChangeset, log: &XprLog) {
        let mut values_changed = XidSet::new();
        for change in changes.node_updates.iter() {
            match change {
            xprdb::XprChange::ExpressionUpdate(node, xid) => {
                if *node == self.node_id {
                    if let Some(expression) = self.db.get_expression(xid) {
                        let deps = self.db.get_dependencies(xid);
                        if self.xpr_set.is_local_xid(&xid) {
                            self.xpr_set.remove_xpr(xid, &mut values_changed, log);
                        }
                        self.xpr_set.insert_xpr(xid.clone(), expression.to_string(), deps, &mut values_changed, log);
                    } else {
                        self.xpr_set.remove_xpr(xid, &mut values_changed, log);
                    }
                } else {
                    // hmmm should really only be sending new clients
                    self.send_clients_changed(node, log);

                    let msg = xprmessage::Message::Expression{xid: xid.clone(), expression:
                                self.build_opt_expression_message(xid)};
                    if let Some(client) = self.clients.get_mut(node) {
                        client.connection.send(&msg, log);
                    }
                }
            },
            xprdb::XprChange::InterestUpdate(node, xid) => {
                if *node == self.node_id {
                    values_changed.push(*xid);
                } else {
                    // hmmm should really only be sending new clients
                    self.send_clients_changed(node, log);

                    let msg = xprmessage::Message::Expression{xid: xid.clone(), expression:
                                self.build_opt_expression_message(xid)};
                    if let Some(client) = self.clients.get_mut(node) {
                        client.connection.send(&msg, log);
                    }
                }
            },
            }
        }

        self.broadcast_value_changes(&values_changed, log);
    }

    /* diag */

    pub fn diag_db(self: &Self) {
        self.db.diag_db();
    }

    pub fn diag_db_web(self: &Self) -> String{
        self.db.diag_db_web()
    }

    pub fn status_clients(self: &Self, log: &XprLog) {
        for k in self.clients.keys() {
            if *k == self.node_id {
                xlog!(log, "Server: {}", k)
            } else {
                xlog!(log, "Client: {}", k)
            }
        }
    }

    pub fn show(self: &Self, log: &XprLog) {
        self.xpr_set.show(log);
    }

    pub fn web(self: &Self) -> String {
        self.xpr_set.web()
    }

    /* utility */

    pub fn read_nodeid(s: &str, end: &mut usize) -> Option<String> {
        match s.find(|c: char| (c < 'a') || (c > 'z')) {
            Some(i) => match s.get(..i) {
                Some(ss) => {
                    *end = i;
                    Some(ss.to_string())
                },
                None => {
                    *end = 0;
                    None
                }
            },
            None => if s.len() > 0 {
                *end = s.len();
                Some(s.to_string())
            } else {
                *end = 0;
                None
            }
        }
    }

    pub fn get_value_change(self: &Self) -> NotifyInc {
        self.value_change.clone()
    }

    pub fn get_client_change(self: &Self) -> NotifyInc {
        self.client_change.clone()
    }
}

