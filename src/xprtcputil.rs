use super::xprlog::XprLog;
use super::xlog;
use super::xprconnection;
use super::xprmessage;
use std::io::Result;

// sort_to_unique is useful for combining two node lists
// and finding the union with an insertion sort.
pub fn sort_to_unique(list: &mut Vec<String>) {
    let mut candidate = 1;
    while candidate < list.len() {
        let mut target = 0;
        let mut dropped = false;
        while target < candidate {
            if list[target].gt(&list[candidate]) {
                let tmp = std::mem::take(&mut list[candidate]);
                let mut moveto = candidate;
                while moveto > target {
                    list[moveto] = std::mem::take(&mut list[moveto-1]);
                    moveto -= 1;
                }
                list[moveto] = tmp;
                break;
            } else if list[target].eq(&list[candidate]) {
                dropped = true; // don't need this value, only want one of each
                let last = list.len()-1;
                list[candidate] = std::mem::take(&mut list[last]);
                list.pop(); // discard extra value
                break;
            } else {
                // sort order ok as is
                target += 1;
            }
        }

        if !dropped {
            candidate+=1;
        }
    }
}

#[cfg(test)]
mod tests {
    fn test_sort(a: Vec<&str>, b: Vec<&str>) {
        use super::sort_to_unique;
        let mut sa : Vec<String> = a.iter().map(|x| x.to_string()).collect();
        let sb : Vec<String> = b.iter().map(|x| x.to_string()).collect();

        sort_to_unique(&mut sa);
        assert_eq!(&sa, &sb);
    }

    #[test]
    pub fn test_sort_to_unique() {
        test_sort(vec![], vec![]);
        test_sort(vec!["a"], vec!["a"]);
        test_sort(vec!["a", "b"], vec!["a", "b"]);
        test_sort(vec!["b", "a"], vec!["a", "b"]);
        test_sort(vec!["a", "b", "c"], vec!["a", "b", "c"]);
        test_sort(vec!["a", "c", "b"], vec!["a", "b", "c"]);
        test_sort(vec!["b", "a", "c"], vec!["a", "b", "c"]);
        test_sort(vec!["b", "c", "a"], vec!["a", "b", "c"]);
        test_sort(vec!["c", "b", "a"], vec!["a", "b", "c"]);
        test_sort(vec!["c", "a", "b"], vec!["a", "b", "c"]);

        test_sort(vec!["a", "a"], vec!["a"]);
        test_sort(vec!["a", "a", "a"], vec!["a"]);

        test_sort(vec!["a", "a", "b"], vec!["a", "b"]);
        test_sort(vec!["a", "b", "a"], vec!["a", "b"]);
        test_sort(vec!["a", "b", "b"], vec!["a", "b"]);
        test_sort(vec!["b", "a", "a"], vec!["a", "b"]);
        test_sort(vec!["b", "a", "b"], vec!["a", "b"]);
        test_sort(vec!["b", "b", "a"], vec!["a", "b"]);

        test_sort(vec!["a", "a", "b", "b"], vec!["a", "b"]);
        test_sort(vec!["b", "a", "b", "a"], vec!["a", "b"]);
        test_sort(vec!["b", "c", "c", "a", "c", "b", "c", "a"], vec!["a", "b", "c"]);
    }
}


pub fn client_port(name: &str) -> u16 {
    let mut port : u16 = 7000-('a' as u16);
    let mut mult = 1;
    for c in name.bytes() {
        port += c as u16 * mult;
        mult *= 10;
    }

    port
}

pub fn client_web_port(name: &str) -> u16 {
    client_port(name) + 1000
}

pub fn json_str_from_input(input: &std::vec::Vec<u8>) -> Result<&str> {
    let mut len = input.len();
    while len > 0 && input[len-1] == xprconnection::JSON_SEP {
        len -= 1;
    }
    match std::str::from_utf8(&input[..len]) {
    Ok(s) => Ok(s),
    Err(e) => Err(std::io::Error::new(std::io::ErrorKind::InvalidInput, e.to_string())),
    }
}

pub fn generate_log_web_table(log: &XprLog) -> String {
    format!("<table hx-get='/lognext?inc={}' hx-swap='outerHTML' hx-trigger='load'>{}</table>\
        <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>",
        log.get_changed().get_inc(), log.web_table_rows())
}


// XprTaskGroup allows for a collection of sync tasks
// to cancel/stop together and await their termination (for data safety).
// [Non-Orphan] Tasks will cancel the group on termination,
// Orphan tasks will not cancel others but stop with the others.
// All spawned tasks will be waited to terminate with closed/stop.

// Note that Tasks will only notice a cancel request if all async [await]
// operations also select! the cancellation token which is easier to do with
// await_ macros (accept().await; calls can last forever otherwise).
pub struct XprTaskGroup {
    pub tracker: tokio_util::task::TaskTracker,
    pub cancel: tokio_util::sync::CancellationToken,
    pub drop_guard: tokio_util::sync::DropGuard,
}

pub struct XprTask {
    pub cancel: tokio_util::sync::CancellationToken,
    pub drop_guard: tokio_util::sync::DropGuard,
}

pub struct XprOrphanTask {
    pub cancel: tokio_util::sync::CancellationToken,
}

impl XprTaskGroup {
    pub fn new() -> XprTaskGroup {
        let tracker = tokio_util::task::TaskTracker::new();
        let cancel = tokio_util::sync::CancellationToken::new();
        let drop_guard = cancel.clone().drop_guard();
        XprTaskGroup{ tracker: tracker, cancel: cancel, drop_guard: drop_guard }
    }

    pub fn spawn<F>(&self, task: F)
        where
            F: core::future::Future + Send + 'static,
            F::Output: Send + 'static,
    {
        self.tracker.spawn(task);
    }

    pub fn task(self: &Self) -> XprTask {
        XprTask{ cancel: self.cancel.clone(), drop_guard: self.cancel.clone().drop_guard() }
    }
    pub fn orphan_task(self: &Self) -> XprOrphanTask {
        XprOrphanTask{ cancel: self.cancel.clone() }
    }

    pub async fn closed(self: &Self) {
        self.tracker.close();
        self.tracker.wait().await
    }

    pub async fn stop(self: &Self) {
        self.cancel.cancel();
        self.closed().await;
    }
}

impl XprTask {
    pub async fn try_wait<F: core::future::Future>(self: &Self, future: F) -> Option<<F as core::future::Future>::Output> {
        tokio::select! {
            _ = self.cancel.cancelled() => {
                None
            }
            ret = future => {
                Some(ret)
            }
        }
    }

    pub fn child_group(self: &Self) -> XprTaskGroup {
        let tracker = tokio_util::task::TaskTracker::new();
        let cancel = self.cancel.child_token();
        let drop_guard = cancel.clone().drop_guard();
        XprTaskGroup{ tracker: tracker, cancel: cancel, drop_guard: drop_guard }
    }
}

impl XprOrphanTask {
    pub async fn try_wait<F: core::future::Future>(self: &Self, future: F) -> Option<<F as core::future::Future>::Output> {
        tokio::select! {
            _ = self.cancel.cancelled() => {
                None
            }
            ret = future => {
                Some(ret)
            }
        }
    }

    pub fn child_group(self: &Self) -> XprTaskGroup {
        let tracker = tokio_util::task::TaskTracker::new();
        let cancel = self.cancel.child_token();
        let drop_guard = cancel.clone().drop_guard();
        XprTaskGroup{ tracker: tracker, cancel: cancel, drop_guard: drop_guard }
    }
}

// await_* macros make using XprTasks to cancel async operations a breeze

#[macro_export]
macro_rules! await_return {
    ( $t:expr, $e:expr ) => {
        match $t.try_wait($e).await {
            Some(x) => x,
            None => return,
        }
    }
}

#[macro_export]
macro_rules! await_break {
    ( $t:expr, $e:expr ) => {
        match $t.try_wait($e).await {
            Some(x) => x,
            None => break,
        }
    }
}

#[macro_export]
macro_rules! await_void {
    ( $t:expr, $e:expr ) => {
        match $t.try_wait($e).await {
            Some(_x) => (()),
            None => (()),
        }
    }
}

#[macro_export]
macro_rules! await_none {
    ( $t:expr, $e:expr ) => {
        match $t.try_wait($e).await {
            Some(x) => x,
            None => return None,
        }
    }
}

pub async fn receive_message(client_num: u64, input: &mut std::vec::Vec<u8>,
                                reader: &mut tokio::io::BufReader<tokio::net::tcp::OwnedReadHalf>,
                                task: &tokio_util::sync::CancellationToken,
                                log: &XprLog) -> Option<xprmessage::Message> {
    use tokio::io::AsyncBufReadExt;

    input.clear();

    let res = tokio::select! {
            _ = task.cancelled() => {
                return None
            }
            ret = reader.read_until(xprconnection::JSON_SEP, input) => {
                ret
            }
        };

    // read '\0' terminated json to convert into xprmessages
    if let Err(e) = res {
        xlog!(log, "Client read error {}", e);
        return None;
    }

    if input.len() == 0 {
        xlog!(log, "client {} closed", client_num);
        return None;
    }

    match json_str_from_input(&input) {
    Ok(json_str) => {
        xlog!(log, "client[{}]: {}", client_num, json_str);

        match serde_json::from_str::<xprmessage::Message>(&json_str) {
            Ok(msg) => Some(msg), // got a mesage
            Err(e) => {
                xlog!(log, "Client read error {}", e);
                None
            },
        }
    },
    Err(e) => {
        xlog!(log, "Client utf8 error {}", e);
        None
    },
    }
}

pub async fn forward_tx_to_stream(id: &str, xpr_rx: &mut tokio::sync::mpsc::Receiver<String>,
        mut stream_tx: tokio::net::tcp::OwnedWriteHalf, task: &XprTask, log: &XprLog) {
    use tokio::io::AsyncWriteExt;

    loop {
        match await_return!(task, xpr_rx.recv()) {
            Some(msg) => {
                if let Err(e) =  stream_tx.write_all(&msg.as_bytes()).await {
                    xlog!(log, "Client {} write failed {}", id, e);
                    if let Err(e) = stream_tx.shutdown().await {
                        xlog!(log, "Client {} shutdown failed {}", id, e);
                    }
                    return;
                }
            },
            None => {
                xlog!(log, "Client {} mpsc closed", id);
                if let Err(e) = stream_tx.shutdown().await {
                    xlog!(log, "Client {} shutdown failed {}", id, e);
                }
                return;
            },
        }
    }
}

