// XprTcpServer host an XprServer providing async TCP access to it using Tokio.
// Async tasks are spawned for the following:
//   - receiving HTTP diagnostic requests
//   - receiving incoming connections from clients
// They will run until their TaskGroup is stopped or one fails

use super::xprlog::XprLog;
use super::xlog;
use super::xprserver;
use super::xpression;
use crate::xprtcputil::client_port;
use crate::xprtcputil::client_web_port;
use crate::xprtcputil::generate_log_web_table;
use crate::xprtcputil::receive_message;
use crate::xprtcputil::sort_to_unique;
use crate::xprtcputil::forward_tx_to_stream;
use super::xprtcputil::XprTask;
use super::xprtcputil::XprTaskGroup;
use super::await_return;
use super::await_break;

pub struct XprTcpServer {
    xsam: std::sync::Arc<std::sync::Mutex<xprserver::XprServer>>,
    log: XprLog,

}

struct AddedClient {
    group: XprTaskGroup,
}

impl XprTcpServer {
    fn generate_server_web_page_xpressions(xsam: &std::sync::Arc<std::sync::Mutex<xprserver::XprServer>>) -> String {
        let server = xsam.lock().unwrap();
        format!("<div hx-get='/xprs?inc={}' hx-swap='outerHTML' hx-trigger='load'>{}</div>", server.get_value_change().get_inc(), server.web())
    }

    fn generate_server_web_page_client_table(xsam: &std::sync::Arc<std::sync::Mutex<xprserver::XprServer>>,
        clientsam: &std::sync::Arc<std::sync::Mutex<std::collections::HashMap<String, AddedClient>>>) -> String {

        let server = xsam.lock().unwrap();
        let connected_clients : Vec<String> = server.get_clients();
        let created_clients : Vec<String> = clientsam.lock().unwrap().keys().cloned().collect();
        let mut clients = Vec::new();
        clients.append(&mut connected_clients.clone());
        clients.append(&mut created_clients.clone());
        sort_to_unique(&mut clients);

        let mut client_table = String::new();
        if !clients.is_empty() {
            client_table += "<table border='1'>";
            for node in clients.iter() {
                let port = client_web_port(node);
                let del = if created_clients.contains(node) {
                    format!(r##"<td><form target="_blank" method="post" action="./rmclient?clientnode={}"><input type="submit" value="Remove"></form></td>"##, node)
                } else {
                    "<td></td>".to_string()
                };

                if connected_clients.contains(node) {
                    let port_str = match server.get_client_port(node) { Some(port) => port.to_string(), None => "No Port".to_string() };
                    client_table += &format!("<tr><td>{} {}</td><td><a href='http://localhost:{}'>http://localhost:{}</a></td>{}</tr>", node, port_str, port, port, del);
                } else {
                    client_table += &format!("<tr><td>{}</td><td>offline</td>{}</tr>", node,  del);
                }
            }
            client_table += "</table>";
        }
        format!("<div hx-get='/clients?inc={}' hx-swap='outerHTML' hx-trigger='load'>{}</div>", server.get_client_change().get_inc(), client_table)
    }

    fn generate_server_web_page(xsam: &std::sync::Arc<std::sync::Mutex<xprserver::XprServer>>,
        clientsam: &std::sync::Arc<std::sync::Mutex<std::collections::HashMap<String, AddedClient>>>) -> String {
        let expression_div = Self::generate_server_web_page_xpressions(xsam);
        let client_div = Self::generate_server_web_page_client_table(xsam, clientsam);
        let server = xsam.lock().unwrap();

        format!(
            r##"<html><head><script src='https://unpkg.com/htmx.org@1.9.10'></script></head>
<h1>Server [{}]  status</h1>
<a href='/log'>log</a>
<a href='/dbdiag'>db diagnostics</a>
<br/>
    <div style="border: 1px solid #000000; display: inline-block;">
    <h2>Xpressions</h2>
    {}
    <form hx-post="/xprmod" hx-swap="innerHTML" hx-target="#xprmodresult">
        <input name="xprid" type="text">
        <input name="xpression" type="text">
        <br>
        <input type="submit" name="xpractionadd" value="Add">
        <input type="submit" name="xpractionrm" value="Remove">
        <input type="submit" name="xpractionch" value="Change">
        <br>
        <span id="xprmodresult"></span>
    </form>
    </div>
<br/>
    <!--open new client-->
    <div style="border: 1px solid #000000; display: inline-block;">
    <h2>Clients</h2>
    <!--client links-->
    {}
    <div>
    <form hx-post="/addclient" hx-swap="innerHTML" hx-target="#addclientresult">
        <input name="clientnode" type="text">
        <input type="submit" name="clientaction" value="Add client">
        <br>
        <span id="addclientresult"></span>
    </form>
    </div>
</html>"##,
server.get_node(), expression_div, client_div)
    }

    fn server_web_add_client(name: &str,
        xsam: &std::sync::Arc<std::sync::Mutex<xprserver::XprServer>>,
        clientsam: &std::sync::Arc<std::sync::Mutex<std::collections::HashMap<String, AddedClient>>>,
        server_log: &XprLog) -> String {

        let mut clients = clientsam.lock().unwrap();
        let server_name : String;

        if clients.contains_key(name) {
            return "Client already created".to_string();
        } else {
            let server = xsam.lock().unwrap();
            let connected_clients = server.get_clients();
            if connected_clients.contains(&name.to_string()) {
                return "Client already connected".to_string();
            }
            server_name = server.get_node();
        }

        let mut log = server_log.clone();
        log.set_prefix(format!("{}> ", name));
        let mut client = super::xprtcpclient::XprTcpClient::new(name, &server_name, log);
        let group = XprTaskGroup::new();
        let client_task = group.task();

        group.spawn(async move {
            let client_group = client_task.child_group();
            client.run_task(&client_group).await;
            client_group.closed().await;
        });

        clients.insert(name.to_string(), AddedClient{ group: group });
        let web_port = client_web_port(name);
        format!("Client {} created <a href='http://localhost:{}'>http://localhost:{}</a>", name, web_port, web_port)
    }

    fn spawn_server_web_client(xsam: std::sync::Arc<std::sync::Mutex<xprserver::XprServer>>,
        stream: tokio::net::TcpStream,
        clientsam: std::sync::Arc<std::sync::Mutex<std::collections::HashMap<String, AddedClient>>>,
        log: &XprLog) {
        use std::os::unix::io::AsRawFd;

        xlog!(log, "web client {} accepted", stream.as_raw_fd() as u64);
        let client_log = log.clone();
        tokio::spawn(async move {
            let service_xsam = &xsam.clone();
            let service_log = &client_log;
            let service_clientsam = &clientsam.clone();
            let service = hyper::service::service_fn(
                |req: hyper::Request<hyper::body::Incoming>| async move {
                    if req.version() == hyper::Version::HTTP_11 {
                        if req.uri().path() == "/test" {
                            let content = "<html>\
                            <script src='https://unpkg.com/htmx.org@1.9.10'></script>\
                            <p>Hello from HTTP test</p>\
                            <p hx-get='/time' hx-swap='outerHTML' hx-trigger='load'>Loading</p>\
                            </html>";
                            Ok(hyper::Response::new(http_body_util::Full::<hyper::body::Bytes>::from(content)))
                        } else if req.uri().path()  == "/time" {
                            tokio::time::sleep(tokio::time::Duration::from_millis(1000)).await;
                            let content = format!("<p hx-get='/time' hx-swap='outerHTML' hx-trigger='load'>{}</p>", chrono::Utc::now());
                            Ok(hyper::Response::new(http_body_util::Full::<hyper::body::Bytes>::from(content)))
                        } else if req.uri().path() == "/log" {
                            let content = format!("<html><head>\
                            <script src='https://unpkg.com/htmx.org@1.9.10'></script>\
                            </head><body>\
                            <h1>Server log</h1>{}\
                            </body></html>", generate_log_web_table(service_log));
                            Ok(hyper::Response::new(http_body_util::Full::<hyper::body::Bytes>::from(content)))
                        } else if req.uri().path() == "/clients" {
                            let cc = service_xsam.lock().unwrap().get_client_change();
                            if let Some(q) = req.uri().query() {
                                if let Some(incstrindex) = q.find("inc=") {
                                    if let Ok(inc) = q[incstrindex+"inc=".len()..].parse::<u64>() {
                                        cc.wait_from(inc).await;
                                    } else {
                                        cc.wait().await;
                                    }
                                } else {
                                    cc.wait().await;
                                }
                            } else {
                                cc.wait().await;
                            }
                            let content = Self::generate_server_web_page_client_table(service_xsam, service_clientsam);
                            Ok(hyper::Response::new(http_body_util::Full::<hyper::body::Bytes>::from(content)))
                        } else if req.uri().path() == "/xprs" {
                            let vc = service_xsam.lock().unwrap().get_value_change();
                            if let Some(q) = req.uri().query() {
                                if let Some(incstrindex) = q.find("inc=") {
                                    if let Ok(inc) = q[incstrindex+"inc=".len()..].parse::<u64>() {
                                        vc.wait_from(inc).await;
                                    } else {
                                        vc.wait().await;
                                    }
                                } else {
                                    vc.wait().await;
                                }
                            } else {
                                vc.wait().await;
                            }
                            let content = Self::generate_server_web_page_xpressions(service_xsam);
                            Ok(hyper::Response::new(http_body_util::Full::<hyper::body::Bytes>::from(content)))
                        } else if req.uri().path() == "/lognext" {
                            let lc = service_log.get_changed();
                            if let Some(q) = req.uri().query() {
                                if let Some(incstrindex) = q.find("inc=") {
                                    if let Ok(inc) = q[incstrindex+"inc=".len()..].parse::<u64>() {
                                        lc.wait_from(inc).await;
                                    } else {
                                        lc.wait().await;
                                    }
                                } else {
                                    lc.wait().await;
                                }
                            } else {
                                lc.wait().await;
                            }
                            let content = generate_log_web_table(service_log);
                            Ok(hyper::Response::new(http_body_util::Full::<hyper::body::Bytes>::from(content)))
                        } else if req.uri().path() == "/dbdiag" {
                            let content = format!("<html><body><h1>Db diagnostics</h1><p>{}</p></body></html>", service_xsam.lock().unwrap().diag_db_web());
                            Ok(hyper::Response::new(http_body_util::Full::<hyper::body::Bytes>::from(content)))
                        } else if req.uri().path() == "/rmclient" {
                            if req.method() == hyper::Method::POST {
                                let (parts, _body_stream) = req.into_parts();
                                if let Some(query) = parts.uri.query() {
                                    if let Some(nodestrindex) = query.find("clientnode=") {
                                        let mut end : usize = 0;
                                        if let Some(nodeid) = xprserver::XprServer::read_nodeid(&query[nodestrindex+"clientnode=".len()..], &mut end) {
                                            let opt_clients = service_clientsam.lock().unwrap().remove(&nodeid);
                                            if let Some(client) = opt_clients {
                                                client.group.cancel.cancel();
                                                xlog!(service_log, "Client {} group dying", &nodeid);
                                                let result = client.group.closed().await;
                                                xlog!(service_log, "Client {} group dead", &nodeid);
                                                Ok(hyper::Response::new(http_body_util::Full::<hyper::body::Bytes>::from(format!("<html><body><h1>Client {} removed</h1><p>{:?}</p><p>{:?}</p>/body></html>", nodeid, parts, result))))
                                            } else {
                                                Ok(hyper::Response::new(http_body_util::Full::<hyper::body::Bytes>::from(format!("Client {} not present {:?}", nodeid, parts))))
                                            }
                                        } else {
                                            Ok(hyper::Response::new(http_body_util::Full::<hyper::body::Bytes>::from("Bad data")))
                                        }
                                    } else {
                                        Ok(hyper::Response::new(http_body_util::Full::<hyper::body::Bytes>::from("Bad data")))
                                    }
                                } else {
                                    Ok(hyper::Response::new(http_body_util::Full::<hyper::body::Bytes>::from("Bad data")))
                                }
                            } else {
                                Ok(hyper::Response::new(http_body_util::Full::<hyper::body::Bytes>::from("Only respond to POST request")))
                            }
                        } else if req.uri().path() == "/xprmod" {
                            if req.method() == hyper::Method::POST {
                                use http_body_util::BodyExt;
                                if let Ok(body) = req.collect().await {
                                    let body_bytes = body.to_bytes().to_vec();
                                    let body_string = String::from_utf8(body_bytes.to_vec()).unwrap();
                                    let mut xprid = String::new();
                                    let mut xpression = String::new();
                                    let mut action = String::new();
                                    for s in body_string.split('&') {
                                        if s.starts_with("xpression=") {
                                            xpression = s["xpression=".len()..].to_string();
                                            xpression = xpression.replace("%2B", "+");
                                            xpression = xpression.replace("%2C", ",");
                                            xpression = xpression.replace("%23", "#");
                                        } else if s.starts_with("xprid=") {
                                            xprid = s["xprid=".len()..].to_string();
                                        } else if s.starts_with("xpractionadd") {
                                            action.push_str("add");
                                        } else if s.starts_with("xpractionrm") {
                                            action.push_str("remove")
                                        } else if s.starts_with("xpractionch") {
                                            action.push_str("change")
                                        }
                                    }

                                    let mut end : usize = 0;
                                    let node = xprserver::XprServer::read_nodeid(&xprid, &mut end).unwrap_or("".to_string());
                                    let content = if let Some(xid) = xpression::read_number(xprid.get(end..).unwrap(), &mut end) {
                                        if end + node.len() != xprid.len() {
                                            "Characters trailing xpression id".to_string()
                                        } else {
                                            match action.as_str() {
                                                "add" => match service_xsam.lock().unwrap().add_xpr(xid, &xpression, &node, service_log) {
                                                    Ok(()) => format!("Xpression {} added", xid),
                                                    Err(s) => format!("Failed to add xpression: {}", s),
                                                },
                                                "remove" => match service_xsam.lock().unwrap().rm_xpr(&xid, service_log) {
                                                    Ok(()) => format!("Xpression {} removed", xid),
                                                    Err(s) => format!("Failed to remove xpression: {}", s),
                                                },
                                                "change" => match service_xsam.lock().unwrap().ch_xpr(&xid, &xpression, service_log) {
                                                    Ok(()) => format!("Xpression {} changed", xid),
                                                    Err(s) => format!("Failed to change xpression: {}", s),
                                                },
                                                _ => "Bad Request".to_string()
                                            }
                                        }
                                    } else {
                                        "Could not parse xpression id".to_string()
                                    };

                                    Ok(hyper::Response::new(http_body_util::Full::<hyper::body::Bytes>::from(content + " BODY:" + &body_string)))
                                } else {
                                    Ok(hyper::Response::new(http_body_util::Full::<hyper::body::Bytes>::from("Bad data")))
                                }
                            } else {
                                Ok(hyper::Response::new(http_body_util::Full::<hyper::body::Bytes>::from("Only respond to POST request")))
                            }
                        } else if req.uri().path() == "/addclient" {
                            if req.method() == hyper::Method::POST {
                                use http_body_util::BodyExt;
                                if let Ok(body) = req.collect().await {
                                    let body_bytes = body.to_bytes().to_vec();
                                    let body_string = String::from_utf8(body_bytes.to_vec()).unwrap();
                                    if let Some(nodestrindex) = body_string.find("clientnode=") {
                                        let mut end : usize = 0;
                                        if let Some(nodeid) = xprserver::XprServer::read_nodeid(&body_string[nodestrindex+"clientnode=".len()..], &mut end) {
                                            let content = Self::server_web_add_client(&nodeid, service_xsam, service_clientsam, service_log);
                                            Ok(hyper::Response::new(http_body_util::Full::<hyper::body::Bytes>::from(content + " BODY:" + &body_string)))
                                        } else {
                                            Ok(hyper::Response::new(http_body_util::Full::<hyper::body::Bytes>::from("Bad data")))
                                        }
                                    } else {
                                        Ok(hyper::Response::new(http_body_util::Full::<hyper::body::Bytes>::from("Bad data")))
                                    }
                                } else {
                                    Ok(hyper::Response::new(http_body_util::Full::<hyper::body::Bytes>::from("Bad data")))
                                }
                            } else {
                                Ok(hyper::Response::new(http_body_util::Full::<hyper::body::Bytes>::from("Only respond to POST request")))
                            }
                        } else {
                            let content = Self::generate_server_web_page(service_xsam, service_clientsam);
                            Ok(hyper::Response::new(http_body_util::Full::<hyper::body::Bytes>::from(content)))
                        }
                    } else {
                        // Note: it's usually better to return a Response
                        // with an appropriate StatusCode instead of an Err.
                        Err("not HTTP/1.1, abort connection")
                    }
                });

            if let Err(err) = hyper::server::conn::http1::Builder::new()
                .serve_connection(hyper_util::rt::TokioIo::new(stream), service)
                    .await
            {
                xlog!(client_log, "Error serving connection: {:?}", err);
            }
        });
    }

    pub fn spawn_server_web(xsam: &std::sync::Arc<std::sync::Mutex<xprserver::XprServer>>,
        task_group: &XprTaskGroup, log: &XprLog) {
        let listener_xsam = xsam.clone();
        let listener_log = log.clone();
        let web_port = client_web_port(&xsam.lock().unwrap().get_node());
        let clientsam = std::sync::Arc::new(std::sync::Mutex::new(std::collections::HashMap::new()));
        let web_task = task_group.task();

        task_group.spawn(async move {
            let address = format!("localhost:{}", web_port);
            let listener = match await_return!(web_task, tokio::net::TcpListener::bind(address.clone())) {
                Ok(bound) => bound,
                Err(e) => { xlog!(listener_log, "Web Listen Error {}",e); return; },
            };
            xlog!(listener_log, "web listener bound {}", address);
            loop {
                let ( stream, _) = match await_break!(web_task, listener.accept()) {
                    Ok(accepted) => accepted,
                    Err(e) => { xlog!(listener_log, "Accept Error {}",e); return; },
                };

                Self::spawn_server_web_client(listener_xsam.clone(), stream, clientsam.clone(), &listener_log);
            }
            xlog!(listener_log, "spawn_server_web dead");
        });
    }

    async fn handle_client_messages(xsam: &std::sync::Arc<std::sync::Mutex<xprserver::XprServer>>,
        client_num: u64, id: &str, mut reader: tokio::io::BufReader<tokio::net::tcp::OwnedReadHalf>,
        task: &XprTask, log: &XprLog) {
        let mut input = Vec::new();
        loop {
            match receive_message(client_num, &mut input, &mut reader, &task.cancel, log).await {
                Some(msg) => {
                    xsam.lock().unwrap().handle_client_msg(id, &msg, log);
                },
                None => {
                    xsam.lock().unwrap().handle_client_closed(id, log);
                    return;
                },
            }
        }
    }

    pub fn spawn_server_p2p_client(xsam: &std::sync::Arc<std::sync::Mutex<xprserver::XprServer>>,
        stream: tokio::net::TcpStream, log: &XprLog) {

        use std::os::unix::io::AsRawFd;
        let client_num = stream.as_raw_fd() as u64;
        let client_xsam = xsam.clone();
        let (client_rx, mut client_tx) = stream.into_split();
        let client_log = log.clone();
        xlog!(log, "client {} accepted", client_num);

        tokio::spawn(async move {
            use tokio::io::AsyncWriteExt;
            let mut reader = tokio::io::BufReader::new(client_rx);
            let mut input = Vec::new();

            let group = &XprTaskGroup::new();
            let task = group.task();
            if let Some(msg) = receive_message(client_num, &mut input, &mut reader, &task.cancel, &client_log).await {
                let connect_attempt = client_xsam.lock().unwrap().handle_client_open(&msg, &client_log);
                if let Some(mut connected) = connect_attempt {
                    let connected_id = connected.id.clone();
                    let forward_log = client_log.clone();
                    let forward_task = group.task();
                    group.spawn(async move {
                        forward_tx_to_stream(&connected_id, &mut connected.rx, client_tx, &forward_task, &forward_log).await
                    });

                    Self::handle_client_messages(&client_xsam, client_num, &connected.id, reader, &task, &client_log).await;
                } else {
                    let _ = client_tx.shutdown().await;
                }
            } else {
                let _ = client_tx.shutdown().await;
            }
            group.closed().await;
        });
    }

    pub fn spawn_server_p2p(xsam: &std::sync::Arc<std::sync::Mutex<xprserver::XprServer>>,
        task_group: &XprTaskGroup, log: &XprLog) {
        let port = client_port(&xsam.lock().unwrap().get_node());
        let listener_xsam = xsam.clone();
        let listener_log = log.clone();
        let listener_task = task_group.task();

        task_group.spawn(async move {
            let address = format!("localhost:{}", port);
            let listener = match await_return!(listener_task, tokio::net::TcpListener::bind(address.clone())) {
                Ok(bound) => bound,
                Err(e) => { xlog!(listener_log, "Listen Error {}",e); return; },
            };
            xlog!(listener_log, "listener bound {}", address);
            loop {
                let ( stream, _) = match await_break!(listener_task, listener.accept()) {
                    Ok(accepted) => accepted,
                    Err(e) => { xlog!(listener_log, "Accept Error {}",e); break; },
                };

                Self::spawn_server_p2p_client(&listener_xsam, stream, &listener_log);
            }
            xlog!(listener_log, "spawn_server_p2p dead");
        });
    }

    pub fn new(name: String, log: XprLog) -> std::io::Result<XprTcpServer> {
        let xs = xprserver::XprServer::init(name, &log)?;
        let xsam = std::sync::Arc::new(std::sync::Mutex::new(xs));
        Ok(XprTcpServer{xsam: xsam, log: log})
    }

    pub fn spawn(self: &Self, task_group: &XprTaskGroup) {
        Self::spawn_server_web(&self.xsam, &task_group, &self.log);
        Self::spawn_server_p2p(&self.xsam, &task_group, &self.log);
    }

    pub fn lock(self: &Self) -> std::sync::LockResult<std::sync::MutexGuard<xprserver::XprServer>> {
        self.xsam.lock()
    }
}

