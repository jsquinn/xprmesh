// XprMesh Copyright 2024 Jonathon Quinn
//
// XprMesh is intended to be a test concept of multiple 'nodes' (processes)
// communicating and calculating simple arithmetic expressions which
// can depend on the result of expressions calculated by another node.
//
// A central server 'a' node tracks all expressions and writes configuration
// to storage. Nodes and expressions can be added via the server console
// and expressions are assigned a node to be calculated on.
//
// This project was an informal trial of rust in a reasonable project,
// including non-trival data structures and ownership, async/tokio socket IO,
// writing to files, serde_json serialisation and language independent
// problems such as tracking distributed dependencies (not done well yet),
// IO buffering etc.
//

mod notifyinc;
mod keystrdb;
mod xpression;
mod xprdb;
mod xprlog;
mod xprmessage;
mod xprset;
mod xprconnection;
mod xprserver;
mod xprclient;
mod xprtcputil;
mod xprtcpclient;
mod xprtcpserver;

use std::io::Result;

fn process_input(xs: &mut xprserver::XprServer, input: &str, log: &xprlog::XprLog) -> Result<bool> {
    let cmds : std::vec::Vec<&str> = input.split(' ').filter(|x| x.len() > 0).collect();
    if cmds.len() < 1 {
        return Ok(true);
    }

    if cmds[0] == "help" {
        println!("Welcome to XprMesh");
        println!("addx [node id]<xpr id number> <xpr>");
        println!("rmx <xpr id number>");
        println!("chx <xpr id number> <xpr>");
        println!("status");
        println!("quit");
        println!("Xpressions are prefix notation e.g. '*-7,8,3'");
        return Ok(true);
    }

    if cmds[0] == "exit" || cmds[0] == "quit" {
        println!("Exiting");
        return Ok(false)
    }

    if cmds[0] == "show" {
        xs.show(log);
        return Ok(true);
    }

    if cmds[0] == "diagdb" {
        xs.diag_db();
        return Ok(true);
    }

    if cmds[0] == "status" {
        xs.status_clients(log);
        return Ok(true);
    }

    if cmds.len() == 3 && cmds[0] == "addx" {
        let mut end : usize = 0;
        let node = xprserver::XprServer::read_nodeid(cmds[1], &mut end).unwrap_or("".to_string());
        if let Some(xid) = xpression::read_number(cmds[1].get(end..).unwrap(), &mut end) {
            if end + node.len() != cmds[1].len() {
                println!("Characters trailing xpression id");
            } else {
                match xs.add_xpr(xid, cmds[2], &node, log) {
                    Ok(()) => (),
                    Err(s) => println!("Failed to add xpression: {}", s),
                }
            }
        } else {
            println!("Could not parse xpression id");
        }
        return Ok(true);
    }

    if cmds.len() == 2 && cmds[0] == "rmx" {
        let mut end : usize = 0;
        if let Some(xid) = xpression::read_number(cmds[1], &mut end) {
            if end != cmds[1].len() {
                println!("Characters trailing xpression id");
            } else {
                match xs.rm_xpr(&xid, log) {
                    Ok(()) => (),
                    Err(s) => println!("Failed to add xpression: {}", s),
                }
            }
        } else {
            println!("Could not parse xpression id");
        }
        return Ok(true);
    }

    if cmds.len() == 3 && cmds[0] == "chx" {
        let mut end : usize = 0;
        if let Some(xid) = xpression::read_number(cmds[1], &mut end) {
            if end != cmds[1].len() {
                println!("Characters trailing xpression id");
            } else {
                match xs.ch_xpr(&xid, cmds[2], log) {
                    Ok(()) => (),
                    Err(s) => println!("Failed to change xpression: {}", s),
                }
            }
        } else {
            println!("Could not parse xpression id");
        }
        return Ok(true);
    }

    println!("Input not recognised");

    Ok(true)
}

async fn run_server() -> Result<()> {

    println!("Welcome to XprMesh.");
    let log = xprlog::XprLog::new();

    let server = xprtcpserver::XprTcpServer::new("a".to_string(), log.clone())?;
    let task_group = xprtcputil::XprTaskGroup::new();
    server.spawn(&task_group);

    let mut input = String::new();
    let mut reader = tokio::io::BufReader::new(tokio::io::stdin());
    loop {
        use tokio::io::AsyncBufReadExt;
        reader.read_line(&mut input).await?;

        if !process_input(&mut *server.lock().unwrap(), input.trim(), &log)? {
            break;
        }
        input.clear();
    }
    xlog!(log, "server dying");
    task_group.stop().await;
    xlog!(log, "server dead");
    Ok(())
}

async fn run_client(name: &str, server_name: &str) ->Result<()> {
    let mut client = xprtcpclient::XprTcpClient::new(name, server_name, xprlog::XprLog::new());

    loop {
        client.run_once().await;
        tokio::time::sleep(tokio::time::Duration::from_millis(1000)).await;
    }
}

#[tokio::main]
async fn main() -> Result<()> {
    let mut args = std::env::args();
    let _ = args.next(); // skip process name
    match args.next() {
    None => { run_server().await },
    Some(param) => match param.as_str() {
        "client" => match args.next() {
            None => { run_client("b", "a").await },
            Some(name) => { run_client(name.as_str(), "a").await },
            },
        "server" => { run_server().await },
        _ => Err(std::io::Error::new(std::io::ErrorKind::InvalidInput, "Unexpected run parameter")),
        },
    }
}

