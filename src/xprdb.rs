/*
 * Instead of storing all nodes, xprs and dependencies in server
 * these will be stored in the XprDB and fetched on demand simulating
 * a more scalable persistance oriented solution.
 *
 * DB will store:
 *  xid->expression (1:1)
 *  	xpr_<xid> = <expression>
 *  	node<->xpr relation (1:n)
 *  	relnx_<node>_<xid> = ()
 *  	relxn_<xid> =<node>
 *  xid<->dep xid relations (n:n)
 *  	relxd_<xid>_<dxid> = ()
 *  	reldx_<dxid>_<xid> = ()
 * will be able to query:
 *  xpr's and expressions for a node
 *  dependent node's for a xpr
 *  expression for an xpr
 *  dep's and ped's for an xpr
 *  interested node's for a node?
 *
 * we don't really need a complete list of nodes or xpr in the server
 * because the server can allow any node connection and provide associated
 * xpr on demand.  Server will load its own xpr on startup.
 */

use super::keystrdb;
use super::xpression::Xid;
use super::xpression::XidSet;

pub enum XprChange {
    ExpressionUpdate(String, Xid), //node needs expression update
    InterestUpdate(String, Xid), // node+xid potentially has more/less interested nodes
}

pub struct XprChangeset {
    pub node_updates: std::vec::Vec<XprChange>,
}

pub struct XprDB {
    db: keystrdb::KeyStrDB,
}

impl XprDB {
    pub fn new(path: &std::path::Path) -> std::io::Result<XprDB> {
        let db = keystrdb::KeyStrDB::open_file(&std::path::Path::new(path))?;
        return Ok(XprDB{db: db});
    }

    pub fn diag_db(self: &Self) {
        self.db.diag()
    }

    pub fn diag_db_web(self: &Self) ->String {
        self.db.diag_web()
    }

    fn rm_xpr_err(self: &mut Self, xid: &Xid) -> std::io::Result<XprChangeset>  {
        let mut changes = XprChangeset{ node_updates: std::vec::Vec::new() };

        let found = self.db.remove(&format!("xpr_{}", xid))?;
        if !found {
            return Ok(changes)
        }

        let deps = self.get_dependencies(xid);
        for dep in deps.iter() {
            if let Some(node) = self.get_xid_node(dep) {
                changes.node_updates.push(XprChange::InterestUpdate(node, dep.clone()));
            }
        }

        let node_relation = format!("relxn_{}", xid);
        if let Some(node) = self.db.get(&node_relation) {
            changes.node_updates.push(XprChange::ExpressionUpdate(node.to_string(), xid.clone()));
            self.db.remove(&format!("relnx_{}_{}", node, xid))?;
            self.db.remove(&node_relation)?;
        }

        let prefix = format!("relxd_{}_", xid);
        let mut dep = std::vec::Vec::new();
        for (k, _v) in self.db.get_start_with(&prefix) {
            if let Some(s) = k.strip_prefix(&prefix) {
                if let Ok(did) = s.parse::<Xid>() {
                    dep.push(did);
                }
            }
        }

        for did in dep.iter() {
            self.db.remove(&format!("relxd_{}_{}", xid, did))?;
            self.db.remove(&format!("reldx_{}_{}", did, xid))?;
        }

        Ok(changes)
    }

    pub fn rm_xpr(self: &mut Self, xid: &Xid) -> std::result::Result<XprChangeset, &'static str> {
        match self.rm_xpr_err(xid) {
        Ok(changeset) => Ok(changeset),
        Err(e) => {
                println!("Err {}", e);
                Err("IO error writing to DB")
            },
        }
    }

    fn add_xpr_err(self: &mut Self, xid: Xid, expression: String, node: String, deps: XidSet) -> std::io::Result<XprChangeset> {
        let mut changes = self.rm_xpr_err(&xid)?;
        changes.node_updates.push(XprChange::ExpressionUpdate(node.clone(), xid));

        self.db.insert(format!("xpr_{}", xid), expression)?;
        self.db.insert(format!("relnx_{}_{}", node, xid), String::new())?;
        self.db.insert(format!("relxn_{}", xid), node)?;
        for dep in deps.iter() {
            self.db.insert(format!("relxd_{}_{}", xid, dep), String::new())?;
            self.db.insert(format!("reldx_{}_{}", dep, xid), String::new())?;
            if let Some(dep_node) = self.get_xid_node(dep) {
                changes.node_updates.push(XprChange::InterestUpdate(dep_node, *dep));
            }
        }

        Ok(changes)
    }

    pub fn add_xpr(self: &mut Self, xid: Xid, expression: String, node: String, dep: XidSet) -> std::result::Result<XprChangeset, &'static str> {
        match self.add_xpr_err(xid, expression, node, dep) {
        Ok(changeset) => Ok(changeset),
        Err(e) => {
            println!("Err {}", e);
            Err("IO event writing to DB")
        }}
    }

    pub fn ch_xpr(self: &mut Self, xid: &Xid, expression: String, deps: XidSet) -> std::result::Result<XprChangeset, &'static str>  {
        if let Some(node) = self.get_xid_node(xid) {
            let old_deps = self.get_dependencies(xid);

            if deps.iter().all(|e| old_deps.contains(e))
                     && old_deps.iter().all(|e| deps.contains(e)) {
                // dependencies haven't changed so we can just update the expression
                if let Err(e) = self.db.insert(format!("xpr_{}", xid), expression) {
                    println!("Err {}", e);
                    Err("IO event writing to DB")
                } else {
                    let mut changes = XprChangeset{ node_updates: std::vec::Vec::new() };
                    changes.node_updates.push(XprChange::ExpressionUpdate(node.clone(), xid.clone()));
                    Ok(changes)
                }
            } else {
                self.add_xpr(xid.clone(), expression, node, deps)
            }
        } else {
            Err("Unknown xid")
        }
    }

    pub fn get_node_xids(self: &Self, node: &str) -> XidSet {
        let prefix = format!("relnx_{}_", node);
        let mut xids = std::vec::Vec::new();

        for (k, _v) in self.db.get_start_with(&prefix) {
            if let Some(s) = k.strip_prefix(&prefix) {
                if let Ok(xid) = s.parse::<Xid>() {
                    xids.push(xid);
                }
            }
        }

        xids
    }

    pub fn get_xid_node(self: &Self, xid: &Xid) -> Option<String> {
        self.db.get(&format!("relxn_{}", xid)).map(|s| s.to_string())
    }

    pub fn get_xids_nodeset(self: &Self, xids: &XidSet) -> std::vec::Vec<String> {
        let mut nodes = std::vec::Vec::<String>::new();

        for xid in xids.iter() {
            if let Some(node) = self.get_xid_node(xid) {
                if !nodes.contains(&node) {
                    nodes.push(node);
                }
            }
        }

        nodes
    }

    pub fn get_expression(self: &Self, xid: &Xid) -> Option<String> {
        self.db.get(&format!("xpr_{}", xid)).map(|s| s.to_string())
    }

    pub fn get_expressions(self: &Self, xids: &XidSet) -> std::collections::HashMap<Xid, String> {
        let mut xprs = std::collections::HashMap::new();

        for xid in xids.iter() {
            if let Some(s) = self.db.get(&format!("xpr_{}", xid)) {
                xprs.insert(xid.clone(), s.to_string());
            }
        }

        xprs
    }

    pub fn get_dependencies(self: &Self, xid: &Xid) -> XidSet {
        let prefix = format!("relxd_{}_", xid);
        let mut dids = XidSet::new();

        for (k, _v) in self.db.get_start_with(&prefix) {
            if let Some(s) = k.strip_prefix(&prefix) {
                if let Ok(did) = s.parse::<Xid>() {
                    dids.push(did);
                }
            }
        }

        dids
    }

    pub fn get_dependent_on(self: &Self, xid: &Xid) -> XidSet {
        let prefix = format!("reldx_{}_", xid);
        let mut dons = XidSet::new();

        for (k, _v) in self.db.get_start_with(&prefix) {
            if let Some(s) = k.strip_prefix(&prefix) {
                if let Ok(did) = s.parse::<Xid>() {
                    dons.push(did);
                }
            }
        }

        dons
    }

    pub fn check_not_circular(self: &Self, xid: &Xid, dep: &XidSet, xidstack: &mut XidSet) -> bool {
        xidstack.push(xid.clone());
        for did in dep {
            if xidstack.contains(did) {
                return false;
            }

            if !self.check_not_circular(did, &self.get_dependencies(did), xidstack) {
                return false;
            }
        }
        xidstack.pop();
        true
    }

    pub fn get_dependency_nodes_for_node(self: &Self, node: &str) -> std::vec::Vec<String> {
        let xids = self.get_node_xids(node);
        let mut depnodes = std::vec::Vec::new();
        for xid in xids.iter() {
            let xdeps = self.get_dependencies(xid);
            let xdepnodes = self.get_xids_nodeset(&xdeps);
            for depnode in xdepnodes.iter() {
                if node != depnode && !depnodes.contains(depnode) {
                    depnodes.push(depnode.clone());
                }
            }
        }
        depnodes
    }

    pub fn get_dependent_on_nodes_for_node(self: &Self, node: &str) -> std::vec::Vec<String> {
        let xids = self.get_node_xids(node);
        let mut depnodes = std::vec::Vec::new();
        for xid in xids.iter() {
            let xdeps = self.get_dependent_on(xid);
            let xdepnodes = self.get_xids_nodeset(&xdeps);
            for depnode in xdepnodes.iter() {
                if node != depnode && !depnodes.contains(depnode) {
                    depnodes.push(depnode.clone());
                }
            }
        }
        depnodes
    }

    pub fn get_local_xids_depended_by_node(self: &Self, my_node: &str, target_node: &str) -> XidSet {
        let xids = self.get_node_xids(my_node);
        let target_xids = self.get_node_xids(target_node);
        let mut target_dids = XidSet::new();
        for xid in target_xids.iter() {
            let xdeps = self.get_dependencies(xid);
            for xdep in xdeps.iter() {
                if xids.contains(xdep) && !target_dids.contains(xdep) {
                    target_dids.push(xdep.clone());
                }
            }
        }

        target_dids
    }
}

