use super::xprmessage;
use super::xprlog::XprLog;
use super::xlog;

pub const JSON_SEP : u8 = b'\0';
pub const JSON_SEP_CHAR : char = '\0';

pub enum State {
    Offline,
    Connected{ sender: tokio::sync::mpsc::Sender<String> },
    Failed,
}

pub struct Connection {
    id: String,
    state: State,
}

pub struct Connected {
    pub id: String,
    pub rx: tokio::sync::mpsc::Receiver<String>,
}

impl Connection {
    pub fn init(id: &str) -> Connection {
        Connection{id: id.to_string(), state: State::Offline}
    }

    pub fn open(self: &mut Self, log: &XprLog) ->Option<Connected> {
        match self.state {
            State::Offline => {
                let (tx, rx) = tokio::sync::mpsc::channel::<String>(10);
                self.state = State::Connected{sender: tx};
                xlog!(log, "XprCon {} connected", self.id);

                Some(Connected{id: self.id.clone(), rx: rx})
            },
            State::Connected{sender: _} => {
                xlog!(log, "XprCon {} already connected", self.id);
                None
            },
            State::Failed => {
                xlog!(log, "XprCon {} failing", self.id);
                None
            },
        }
    }

    pub fn close(self: &mut Self, log: &XprLog) {
        match self.state {
            State::Offline => {
                xlog!(log, "XprCon {} already closed", self.id);
            },
            State::Connected{sender: _} => {
                self.state  = State::Offline;
                xlog!(log, "XprCon {} closed", self.id);
            },
            State::Failed => {
                self.state = State::Offline;
                xlog!(log, "XprCon {} closed (failed)", self.id);
            },
        }
    }

    pub fn online(self: &Self) -> bool {
        if let State::Connected{sender: _} = self.state {
            true
        } else {
            false
        }
    }

    pub fn send(self: &mut Self, msg: &xprmessage::Message, log: &XprLog) {
        if let State::Connected{sender: tx} = &self.state {
            let mut line = serde_json::to_string(&msg).unwrap();
            line.push(JSON_SEP_CHAR);
            if let Err(_) = tx.try_send(line) {
                self.state  = State::Failed;
                xlog!(log, "XprConnection {} failed to receive response", self.id);
            }
        }
    }

    pub fn status(self: &Self) -> &str {
        match self.state {
            State::Offline => "offline",
            State::Connected{..} => "connected",
            _ => "failed",
        }
    }
}
