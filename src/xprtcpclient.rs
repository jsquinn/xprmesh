// XprTcpClient host an XprClient providing async TCP access to it using Tokio.
// Multiple Async tasks are spawned for the following:
//   - receiving HTTP diagnostic requests
//   - receiving incoming connections from other clients
//   - managing the list of outgoing connections to other clients
//   - connecting to the server
// These tasks will spawn their own children tasks to handle clients.
// If any of these fail on an error they all will shutdown.

use std::io::Result;
use super::xprlog::XprLog;
use super::xlog;
use super::xprconnection;
use super::xprmessage;
use super::xprclient;
use crate::xprtcputil::client_port;
use crate::xprtcputil::client_web_port;
use crate::xprtcputil::generate_log_web_table;
use crate::xprtcputil::receive_message;
use crate::xprtcputil::forward_tx_to_stream;
use crate::xprtcputil::json_str_from_input;
use super::xprtcputil::XprTaskGroup;
use super::xprtcputil::XprTask;
use super::await_return;
use super::await_break;
use super::await_void;

pub struct XprTcpClient {
    xcam: std::sync::Arc<std::sync::Mutex<xprclient::XprClient>>,
    log: XprLog,
    server_port: u16,
}

impl XprTcpClient {
    fn spawn_client_web_client(task_group: &XprTaskGroup, xcam: std::sync::Arc<std::sync::Mutex<xprclient::XprClient>>,
        stream: tokio::net::TcpStream, log: XprLog) {

        use std::os::unix::io::AsRawFd;
        println!("web client {} accepted", stream.as_raw_fd() as u64);
        let task = task_group.orphan_task();
        let node = xcam.lock().unwrap().get_node();

        task_group.spawn(async move {
            let xcam_ref = &xcam;
            let service_log = &log;
            let service_node = &node;
            let service_task = &task;
            let service = hyper::service::service_fn(
                |req: hyper::Request<hyper::body::Incoming>| async move {
                    let task = service_task;
                    if req.version() == hyper::Version::HTTP_11 {
                        if req.uri().path() == "/test" {
                            let content = "<html><head>\
                            <script src='https://unpkg.com/htmx.org@1.9.10'></script>\
                            </head><body>\
                            <p>Hello from HTTP test</p>\
                            <form method='post'><input name='a' type='text'><input name='b' type='submit'></form>\
                            <p hx-get='/value' hx-swap='outerHTML' hx-trigger='load'>Loading</p>\
                            </body></html>";
                            Ok(hyper::Response::new(http_body_util::Full::<hyper::body::Bytes>::from(content)))
                        } else if req.uri().path()  == "/testvalue" {
                            await_void!(task, tokio::time::sleep(tokio::time::Duration::from_millis(1000)));
                            let content = format!("<p hx-get='/testvalue' hx-swap='outerHTML' hx-trigger='load'>{}</p>", chrono::Utc::now());
                            Ok(hyper::Response::new(http_body_util::Full::<hyper::body::Bytes>::from(content)))
                        } else if req.uri().path() == "/log" {
                            let content = format!("<html><head>\
                            <script src='https://unpkg.com/htmx.org@1.9.10'></script>\
                            </head><body>\
                            <h1>Client log</h1>{}\
                            </body></html>", generate_log_web_table(service_log));
                            Ok(hyper::Response::new(http_body_util::Full::<hyper::body::Bytes>::from(content)))
                        } else if req.uri().path() == "/lognext" {
                            let lc = service_log.get_changed();
                            if let Some(q) = req.uri().query() {
                                if let Some(incstrindex) = q.find("inc=") {
                                    if let Ok(inc) = q[incstrindex+"inc=".len()..].parse::<u64>() {
                                        await_void!(task, lc.wait_from(inc));
                                    } else {
                                        await_void!(task, lc.wait());
                                    }
                                } else {
                                    await_void!(task, lc.wait());
                                }
                            } else {
                                await_void!(task, lc.wait());
                            }
                            let content = generate_log_web_table(service_log);
                            Ok(hyper::Response::new(http_body_util::Full::<hyper::body::Bytes>::from(content)))
                        } else {
                            let status = xcam_ref.lock().unwrap().web();
                            let content = format!("<html><h1>Client {} status</h1>{}<a href='/log'>log</a></html>", service_node, status);
                            Ok(hyper::Response::new(http_body_util::Full::<hyper::body::Bytes>::from(content)))
                        }
                    } else {
                        // Note: it's usually better to return a Response
                        // with an appropriate StatusCode instead of an Err.
                        Err("not HTTP/1.1, abort connection")
                    }
                });

            let mut connection = hyper::server::conn::http1::Builder::new()
                .serve_connection(hyper_util::rt::TokioIo::new(stream), service);

            let connect_pin = std::pin::Pin::new(&mut connection);
            match task.try_wait(async move { connect_pin.await }).await {
                Some(Err(err)) =>{
                    xlog!(log, "Error serving connection: {:?}", err);
                },
                Some(Ok(())) => {},
                None => {
                    //connection.graceful_shutdown();
                    std::pin::Pin::new(&mut connection).graceful_shutdown();
                    xlog!(log, "run_client_web_client dying");
                    // hopefully shuts down
                    let _ = connection.await;
                    xlog!(log, "run_client_web_client dead");
                }
            }
        });
    }

    pub fn spawn_client_web(task_group: &XprTaskGroup, xcam: std::sync::Arc<std::sync::Mutex<xprclient::XprClient>>, log: XprLog) {
        let port = client_web_port(&xcam.lock().unwrap().get_node());
        let task = task_group.task();

        task_group.spawn(async move {
            xlog!(log, "run_client_web spawned");
            let listener = match await_return!(task, tokio::net::TcpListener::bind(format!("localhost:{}", port))) {
                Ok(bound) => bound,
                Err(e) => { xlog!(log, "Web Listen Error {}",e); return; },
            };

            xlog!(log, "web listener bound {}", port);
            let web_group = task.child_group();
            loop {
                let ( stream, _) = match await_break!(task, listener.accept()) {
                    Ok(accepted) => accepted,
                    Err(e) => { xlog!(log, "Accept Error {}",e); return; },
                };

                Self::spawn_client_web_client(&web_group, xcam.clone(), stream, log.clone());
            }
            xlog!(log, "run_client_web group dying");
            web_group.closed().await;
            xlog!(log, "run_client_web dead");
        });
    }

    pub fn spawn_client_p2p_incoming_peer(task_group: &XprTaskGroup,
        xcam: std::sync::Arc<std::sync::Mutex<xprclient::XprClient>>,
        stream: tokio::net::TcpStream,
        peers: std::sync::Arc<std::sync::Mutex<std::collections::HashMap<String, XprTaskGroup>>>,
        log: XprLog)
    {
        use std::os::unix::io::AsRawFd;
        let fd_num = stream.as_raw_fd() as u64;
        let (rx, mut tx) = stream.into_split();
        let task = task_group.orphan_task();

        task_group.spawn(async move {
            use tokio::io::AsyncWriteExt;
            let mut reader = tokio::io::BufReader::new(rx);
            let mut input = Vec::new();

            if let Some(msg) = receive_message(fd_num, &mut input, &mut reader, &task.cancel, &log).await {
                let connect_attempt = xcam.lock().unwrap().handle_peer_in_open(&msg, &log);
                match connect_attempt {
                    Some(id) => {
                        let idc = id.clone();
                        xlog!(log, "peer {} has connected in", id);
                        let existing_peer = peers.lock().unwrap().remove(&idc);
                        if let Some(peer) = existing_peer {
                            peer.cancel.cancel();
                            peer.closed().await;
                        }

                        let group = task.child_group();
                        let task = group.task();
                        group.spawn(async move {
                            let _ = &task;
                            let mut input = Vec::new();
                            loop {
                                match receive_message(fd_num, &mut input, &mut reader, &task.cancel, &log).await {
                                    Some(msg) => {
                                        xcam.lock().unwrap().handle_peer_in_msg(&id, &msg, &log);
                                    },
                                    None => break,
                                }
                            }
                            xcam.lock().unwrap().handle_peer_in_closed(&id, &log);
                            xlog!(log, "peer {} has disconnected", id);
                            let _ = tx.shutdown().await;
                        });
                        peers.lock().unwrap().insert(idc, group);
                    },
                    None => {
                        xlog!(log, "socket {} connection in refused", fd_num);
                        let _ = tx.shutdown().await;
                    },
                }
            }
        });
    }

    pub fn spawn_client_p2p_incoming(task_group: &XprTaskGroup, xcam: std::sync::Arc<std::sync::Mutex<xprclient::XprClient>>, log: XprLog) {
        let port = client_port(&xcam.lock().unwrap().get_node());
        let listener_task = task_group.task();
        task_group.spawn(async move {
            xlog!(log, "run_client_p2p_incoming spawned");
            let listener = match await_return!(listener_task, tokio::net::TcpListener::bind(format!("localhost:{}", port))) {
                Ok(bound) => bound,
                Err(e) => { xlog!(log, "client Listen Error {}",e); return; },
            };

            xlog!(log, "client listener bound {}", port);
            let listener_group = listener_task.child_group();
            {
                let peers = std::sync::Arc::new(std::sync::Mutex::new(std::collections::HashMap::<String, XprTaskGroup>::new()));

                loop {
                    let (stream, from) = match await_break!(listener_task, listener.accept()) {
                        Ok(accepted) => accepted,
                        Err(e) => { xlog!(log, "Accept Error {}",e); return; },
                    };
                    use std::os::unix::io::AsRawFd;
                    let peer_num = stream.as_raw_fd() as u64;
                    xlog!(log, "socket {} accepted from {}", peer_num, from);

                    Self::spawn_client_p2p_incoming_peer(&listener_group,
                        xcam.clone(), stream, peers.clone(), log.clone());
                }

                for (_k, peer) in peers.lock().unwrap().iter_mut() {
                    peer.cancel.cancel();
                }
            }
            xlog!(log, "run_client_p2p_incoming group dying");
            listener_group.closed().await;
            xlog!(log, "run_client_p2p_incoming dead");
        });
    }

    async fn dump_peer_out_rx(stream_rx: tokio::net::tcp::OwnedReadHalf, peer_id: &str, task: &XprTask, log: &XprLog)
    {
        use tokio::io::AsyncBufReadExt;

        let mut input = Vec::new();
        let mut reader = tokio::io::BufReader::new(stream_rx);
        loop {
            if let Err(e) = await_return!(task, reader.read_until(xprconnection::JSON_SEP, &mut input)) {
                xlog!(log, "Peer out read error {}", e);
                return;
            }

            if input.len() == 0 {
                xlog!(log, "Peer out closed {}", peer_id);
                return;
            }

            match json_str_from_input(&input) {
                Ok(json_str) => {
                    xlog!(log, "client out rx? [{}]: {}", peer_id, json_str);
                },
                Err(e) => {
                    xlog!(log, "Client utf8 error {}", e);
                    return;
                },
            }
        }
    }

    pub fn spawn_client_p2p_outgoing_peer(task_group: &XprTaskGroup, xcam: std::sync::Arc<std::sync::Mutex<xprclient::XprClient>>, id: String, log: XprLog) {
        let task = task_group.task();
        xlog!(log, "Peer outgoing {} spawned", id);
        task_group.spawn(async move {
            xlog!(log, "run_client_p2p_outgoing_peer spawned ");

            loop {
                // try connect stream
                let port = xcam.lock().unwrap().get_outgoing_peer_port(&id);
                let dest = &format!("localhost:{}", port);
                let attempt = await_return!(task, tokio::net::TcpStream::connect(dest));
                if let Ok(stream) = attempt {
                    let (rx, mut tx) = stream.into_split();

                    // inform client of connection
                    let connect_attempt = xcam.lock().unwrap().open_outgoing_peer(&id, &log);
                    use tokio::io::AsyncWriteExt;
                    match connect_attempt {
                        Some(mut connected) => {
                            // client accepts
                            xlog!(log, "peer {} connected out to {}", id, dest);

                            let connect_group = task.child_group();

                            // monitor for socket closed
                            let mon_task = connect_group.task();
                            let mon_id = id.clone();
                            let mon_xcam = xcam.clone();
                            let mon_log = log.clone();
                            connect_group.spawn(async move{
                                Self::dump_peer_out_rx(rx, &mon_id, &mon_task, &mon_log).await;
                                mon_xcam.lock().unwrap().close_outgoing_peer(&mon_id, &mon_log);
                            });

                            {
                                // start forwarding messages
                                let forward_task = connect_group.task();
                                forward_tx_to_stream(&id, &mut connected.rx, tx, &forward_task, &log).await;
                            }
                            xlog!(log, "run_client_p2p_outgoing_peer group dying");
                            connect_group.closed().await;
                        },
                        None => {
                            xlog!(log, "peer {} refused connection to {}", id, dest);
                            let _ = tx.shutdown().await;
                        },
                    }
                }

                await_break!(task, tokio::time::sleep(tokio::time::Duration::from_millis(1000)));
            }
            xlog!(log, "run_client_p2p_outgoing_peer dead");
        });
    }

    pub fn spawn_client_p2p_outgoing(task_group: &XprTaskGroup, xcam: std::sync::Arc<std::sync::Mutex<xprclient::XprClient>>, log: XprLog) {
        // listen for id's
        let opt_rx = xcam.lock().unwrap().get_outgoing_peer_list();
        if let Some(mut rx) = opt_rx {
            let task = task_group.task();

            task_group.spawn(async move {
                xlog!(log, "run_client_p2p_outgoing spawned");
                let rx_task_group = task.child_group();
                {
                    let mut peers = std::collections::HashMap::new();
                    loop {
                        let new_peers = rx.borrow_and_update().clone();
                        let mut lost_peers = std::vec::Vec::new();

                        for id in peers.keys() {
                            if !new_peers.contains(id) {
                                lost_peers.push(id.clone());
                            }
                        }

                        for id in lost_peers.iter() {
                            xlog!(log, "Peer outgoing {} dropped", id);
                            peers.remove(id);
                        }

                        for id in new_peers.iter() {
                            if !peers.contains_key(id) {
                                let stream_group = task.child_group();
                                Self::spawn_client_p2p_outgoing_peer(&stream_group, xcam.clone(), id.clone(), log.clone());
                                peers.insert(id.clone(), stream_group);
                            }
                        }

                        if await_break!(task, rx.changed()).is_err() {
                            xlog!(log, "Client outgoing channel closed");
                            break;
                        }
                    }

                    xlog!(log, "run_client_p2p_outgoing peers dying");
                    for (_k, peer) in peers.iter_mut() {
                        peer.cancel.cancel();
                        peer.closed().await;
                    }
                }
                xlog!(log, "run_client_p2p_outgoing group dying");
                rx_task_group.closed().await;
                xlog!(log, "run_client_p2p_outgoing dead");
            });
        }
    }

    pub async fn run_client_once(task: XprTask, xcam: std::sync::Arc<std::sync::Mutex<xprclient::XprClient>>, server_port: u16, log: &XprLog) -> Result<()> {
        use tokio::io::AsyncBufReadExt;
        use tokio::io::AsyncWriteExt;

        let port = client_port(&xcam.lock().unwrap().get_node());
        let stream = match task.try_wait(tokio::net::TcpStream::connect(format!("localhost:{}", server_port))).await {
            Some(estream) => estream?,
            // shutdown already?
            None => return Ok(()),
        };
        let (rx, mut tx) = stream.into_split();

        let opt_connected = xcam.lock().unwrap().open(port, log);
        if let Some(mut connected) = opt_connected {
            let forward_log = log.clone();
            let forward_group = task.child_group();
            let forward_task = forward_group.task();
            forward_group.spawn(async move {
                forward_tx_to_stream(&connected.id, &mut connected.rx, tx, &forward_task, &forward_log).await
            });

            let mut reader = tokio::io::BufReader::new(rx);
            let mut input = Vec::new();
            loop {
                input.clear();
                await_break!(task, reader.read_until(xprconnection::JSON_SEP, &mut input))?;
                if input.len() == 0 {
                    xlog!(log, "connection closed");
                    return Ok(());
                }

                let json_str = json_str_from_input(&input)?;
                xlog!(log, "{}", json_str);
                xcam.lock().unwrap().handle_server_msg(&serde_json::from_str::<xprmessage::Message>(&json_str)?, log);
            }
            xlog!(log, "run_client_once group dying");
            forward_group.closed().await;
        } else {
            let _ = tx.shutdown().await;
        }
        xcam.lock().unwrap().close(log);
        xlog!(log, "run_client_once dead");

        Ok(())
    }

    pub fn new(name: &str, server_name: &str, log: XprLog) -> XprTcpClient {
        let client = xprclient::XprClient::init(name);
        let xcam = std::sync::Arc::new(std::sync::Mutex::new(client));
        XprTcpClient{ xcam: xcam, log: log, server_port: client_port(server_name) }
    }

    pub async fn run_task(self: &mut Self, task_group: &XprTaskGroup) {
        Self::spawn_client_web(&task_group, self.xcam.clone(), self.log.clone());
        Self::spawn_client_p2p_incoming(&task_group, self.xcam.clone(), self.log.clone());
        Self::spawn_client_p2p_outgoing(&task_group, self.xcam.clone(), self.log.clone());

        if let Err(e) = Self::run_client_once(task_group.task(), self.xcam.clone(), self.server_port, &self.log).await {
            xlog!(self.log, "client error {}", e);
        }
    }

    pub async fn run_once(self: &mut Self) {
        let task_group = XprTaskGroup::new();
        self.run_task(&task_group).await;
        task_group.closed().await;
    }
}

